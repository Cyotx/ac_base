<?php
	
	session_start();
	$name = $_SESSION['name'];
	$user = $_SESSION['username'];
	if (!$user){
		header("Location: login.php");
	}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>American Changer Reporter</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="css/AdminLTE.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
      folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="css/skins/_all-skins.min.css">

    <!-- daterange picker -->
    <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
    
    <script src="plugins/daterangepicker/moment.js"></script>
    <script src="plugins/daterangepicker/moment-timezone.js"></script>
    
    <!-- Include Required Prerequisites -->
    <script type="text/javascript" src="js/index2.js"></script></head>
      
    <script type="text/javascript" src="js/jquery.canvasjs.min.js"></script></head>	
    
  </head>
	
<?php
	$name = $_SESSION['name'];
	$user = $_SESSION['username'];
	$operator = $_SESSION['operator'];
	if ($operator == "American Changer" || $operator == "Tests Operator"){
		echo "<body onload=\"loadOperators('".$operator."', 'transaction')\" class=\"hold-transition skin-blue layout-top-nav\">";
		}
		else{
		echo "<body onload=\"loadUser('".$operator."', 'transaction')\" class=\"hold-transition skin-blue layout-top-nav\">";
		}
?>
<div class="wrapper">

  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="dashboard.php">
          <img alt="Brand" src="/AmericanChanger/img/amchanger40.png">
          </a>
          <!--<a href="dashboard.php" class="navbar-brand"><b>American</b>Changer</a> -->
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
          <i class="fa fa-bars"></i>
          </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Reports <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="report_summary.php" data-toggle="tooltip" data-placement="bottom" title="All Sales, Cash and Credit">Sales Summary</a></li>
                <li><a href="report_analysis.php" data-toggle="tooltip" data-placement="bottom" title="Sales Report by Category">Sales Analysis</a></li>
                <li class="divider"></li>
                <li><a href="report_transaction.php" data-toggle="tooltip" data-placement="bottom" title="Last Transactions Registry">Transaction Monitor</a></li>
                <?php
                  $repType = $_SESSION['repType'];
                  if ($repType != 2){
                    echo "<li><a href=\"report_card.php\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"All Transactions by Card Number\">
                    Transactions by Card</a></li>";
                  }
                ?>
                <li class="divider"></li>
                <li><a href="report_totalkiosk.php" data-toggle="tooltip" data-placement="bottom" title="Total Sales By Kiosk">Total Sales By Kiosk</a></li>
                <li><a href="report_totalpkg.php" data-toggle="tooltip" data-placement="bottom" title="Total Sales By Package">Total Sales By Package</a></li>
                <li class="divider"></li>
                <li><a href="report_client_receipt.php" data-toggle="tooltip" data-placement="bottom" title="Get Client Receipt">Get Client Receipt</a></li>
              </ul>
            </li>
            <li> <a href="operations.php" role="button">Operations</a> </li>
            <li> <a href="support.php" role="button">Tech Support</a> </li>
            
          </ul>
        </div>
        <!-- /.navbar-collapse -->
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <li><a href="logout.php">Sign Out</a></li>
          </ul>
        </div>
        <!-- /.navbar-custom-menu -->
      </div>
        <!-- /.container-fluid -->
    </nav>
      
  </header>
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Transaction Monitor
          <!--<small>Example 2.0</small>-->
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Reports</a></li>
          <li><a href="#">Transaction Monitor</a></li>

        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Select Your Search Parameters</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          
          <div class="box-body">
            <div class="col-sm-6">
              <div class="form-group">
                <label class="col-sm-2 control-label">Operator</label>
                <div class="col-sm-10">

                  <div id="selectOperator">
                  </div>
                  <?php
                    if ($operator != "American Changer" && $operator != "Tests Operator"){
                      echo "<select class=\"form-control\" Id=\"operator\">";
                      echo "<option value='" .$operator." '>" .$operator. "</option>";
                      echo "</select>";
                    }
                  ?>
                </div>
              </div>
              <br></br>
              <div class="form-group">
              <label class="col-sm-2 control-label">Kiosk</label>
                <div class="col-sm-10">
                  <div id="selectInfo"><i>Select an Operator</i></div>
                </div>
              </div>
            </div>
            
            <div class="col-sm-6">
              <!-- Date and time range -->
              <div class="form-group">
                <label class="col-sm-2 control-label">Date and Time</label>
                <div class="col-sm-10">
                  <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                    <span id = "datetimeRange"></span> <b class="caret"></b>
                  </div>
                  
                </div>
                <!-- /.input group -->
              </div>
              
              <br></br>
              <div class="form-group">
                <label class="col-sm-3 control-label" for="payment">Payment Method</label>
                <div class="col-sm-9">
                  <label><input type="checkbox" id="cash"> Cash </label>
                  
                  <label><input type="checkbox" id="credit"> Credit Card </label>
                </div>
                <!-- /.input group -->
              </div>
              <div class="form-group row">
                <label class="col-sm-3 control-label" for="transaction_qty">Transaction Qty:</label>
                <select id="transaction_qty">
                  <option value="1">Last</option>
                  <option value="5">Last 10</option>
                  <option value="10">Last 50</option>	
                  <option value="20">Last 100</option>
                  <option value="50">Last 500</option>
                  <option value="100">Last 1000</option>											
                </select>
              </div>
              
            </div>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <button type="submit" class="btn btn-info pull-right" onclick="loadTransactionCanvas()">View Report</button>
          </div>
          <!-- /.box-footer -->
          
        </div>
				<div class="box box-default">
					<div class="box-header with-border">
						<h3 class="box-title">Table Box</h3>
					</div>
					<div class="box-body" id="report_table" style="visibility:hidden">
						<div class="col-xs-12">
							<div class="span3">
								<div id="ReportTransaction"><b>Report info will be listed here...</b></div>
							</div>
						</div> 
					</div>
				</div>
      </section>
      <!--
			<div class="col-xs-offset-2 col-xs-2">
				<div class="form-group">
					<input type="text" class="col-xs-2 form-control pull-right" id="searchBox" placeholder="Search">
					<button class="col-xs-8 btn btn-primary pull-right" onclick="searchTransactions()">Search</button>
				</div>
			</div>-->
    </div>
    <!-- /.container -->
  </div>
  <footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
        <b>Version</b> 2.0
      </div>
      <strong>American Changer Reporter</strong> All rights reserved.
    </div>
  </footer>
  <!--
		<div class="row">
			<div class="col-xs-12" style="background: white">
				<div class="col-xs-12" style="background: white">
					<div class="panel panel-default">
						<div class="panel-body" id="report_table" style="visibility:hidden">
							<h2>Transaction Report</h2>
							<div id="ReportTransaction"><b>Report info will be listed here...</b></div>
						</div>
					</div>
				</div>
			</div>
		</div> -->
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="js/LTE/app.min.js"></script>
<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<script>
  $(function () {
    //$('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
    
    function cb(start, end) {
      $('#reportrange span').html(start.format('YYYY-MM-DD H:mm') + ' - ' + end.format('YYYY-MM-DD H:mm'));
    }
    
    cb(moment().startOf('days'), moment().endOf('days'));
    
    $('#reportrange').daterangepicker({
      timePicker: true,
      timePicker24Hour:true,
      timePickerIncrement: 1,
      
      ranges: {
         'Today': [moment().startOf('days'), moment().endOf('days')],
         'Yesterday': [moment().subtract(1, 'days').startOf('days'), moment().subtract(1, 'days').endOf('days')],
         'Last 7 Days': [moment().subtract(6, 'days').startOf('days'), moment().endOf('days')],
         'Last 30 Days': [moment().subtract(29, 'days').startOf('days'), moment().endOf('days')],
         'This Month': [moment().startOf('month'), moment().endOf('month')],
         'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      }
    }, cb);
  });
</script>
</body>
</html>
