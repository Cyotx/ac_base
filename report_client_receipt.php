<?php
	
	session_start();
	$name = $_SESSION['name'];
	$user = $_SESSION['username'];
	if (!$user){
		header("Location: login.php");
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>American Changer Reporter</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<!-- Bootstrap 3.3.6 -->
		<link rel="stylesheet" href="bootstrap/css/bootstrap.css">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
		<!-- Ionicons -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
		<!-- Theme style -->
		<link rel="stylesheet" href="css/AdminLTE.css">
		<!-- AdminLTE Skins. Choose a skin from the css/skins
		   folder instead of downloading all of them to reduce the load. -->
		<link rel="stylesheet" href="css/skins/_all-skins.min.css">
		


		<!-- daterange picker -->
		<link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
		
		<!-- Include Required Prerequisites -->
		<script type="text/javascript" src="js/index2.js"></script>
		<script type="text/javascript" src="js/jquery.canvasjs.min.js"></script>

	</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->

<?php
	$name = $_SESSION['name'];
	$user = $_SESSION['username'];
	$operator = $_SESSION['operator'];
	if ($operator == "American Changer" || $operator == "Tests Operator"){
		echo "<body onload=\"loadOperators('".$operator."', 'summary')\" class=\"hold-transition skin-blue layout-top-nav\">";
	}
	else{
		echo "<body onload=\"loadUser('".$operator."', 'summary')\" class=\"hold-transition skin-blue layout-top-nav\">";
	}
?>
<div class="wrapper">

  <header class="main-header">
    <nav class="navbar navbar-static-top">
		<div class="container">
			<div class="navbar-header">
			  <a class="navbar-brand" href="dashboard.php">
				<img alt="Brand" src="/AmericanChanger/img/amchanger40.png">
			  </a>
			  <!--<a href="dashboard.php" class="navbar-brand"><b>American</b>Changer</a> -->
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
				<i class="fa fa-bars"></i>
			  </button>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse pull-left" id="navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Reports <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="report_summary.php" data-toggle="tooltip" data-placement="bottom" title="All Sales, Cash and Credit">Sales Summary</a></li>
							<li><a href="report_analysis.php" data-toggle="tooltip" data-placement="bottom" title="Sales Report by Category">Sales Analysis</a></li>
							<li class="divider"></li>
							<li><a href="report_transaction.php" data-toggle="tooltip" data-placement="bottom" title="Last Transactions Registry">Transaction Monitor</a></li>
							<?php
								$repType = $_SESSION['repType'];
								if ($repType != 2){
									echo "<li><a href=\"report_card.php\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"All Transactions by Card Number\">
									Transactions by Card</a></li>";
								}
							?>
							<li class="divider"></li>
							<li><a href="report_totalkiosk.php" data-toggle="tooltip" data-placement="bottom" title="Total Sales By Kiosk">Total Sales By Kiosk</a></li>
							<li><a href="report_totalpkg.php" data-toggle="tooltip" data-placement="bottom" title="Total Sales By Package">Total Sales By Package</a></li>
							<li class="divider"></li>
							<li><a href="report_client_receipt.php" data-toggle="tooltip" data-placement="bottom" title="Get Client Receipt">Get Client Receipt</a></li>
						</ul>
					</li>
					<li> <a href="operations.php" role="button">Operations</a> </li>
					<li> <a href="support.php" role="button">Tech Support</a> </li>
					
				</ul>
				<!--
				<form class="navbar-form navbar-left" role="search">
					<div class="form-group">
						<input type="text" class="form-control" id="navbar-search-input" placeholder="Search">
					</div>
				</form> -->
			</div>
			<!-- /.navbar-collapse -->
			<!-- Navbar Right Menu -->
			<div class="navbar-custom-menu">
				<ul class="nav navbar-nav">
					<li><a href="logout.php">Sign Out</a></li>
				</ul>
			</div>
			<!-- /.navbar-custom-menu -->
		</div>
      <!-- /.container-fluid -->
    </nav>
  </header>
  <!-- Full Width Column -->
	<div class="content-wrapper">
		<div class="container">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
				  Get Client Receipt
				  <!--<small>Example 2.0</small>-->
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Reports</a></li>
					<li><a href="#">Get Client Receipt</a></li>

				</ol>
			</section>

			<!-- Main content -->
			<section class="content">
				
				<div class="box box-primary">
					<div class="box-header with-border">
					  <h3 class="box-title">Insert Client Lastname and credit card last 4 digits.</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					
					<div class="box-body">
						<div class="col-sm-6">
							<div class="form-group">
								<label class="col-sm-2 control-label">Operator</label>
								<div class="col-sm-10">
									<div id="selectOperator">
									</div>
									<?php
										if ($operator != "American Changer" && $operator != "Tests Operator"){
											echo "<select class=\"form-control\" Id=\"operator\">";
											echo "<option value='" .$operator." '>" .$operator. "</option>";
											echo "</select>";
										}
									?>
								</div>
							</div>
						</div>
						
						<div class="col-sm-6">
							<!-- Date and time range -->
							<div class="form-group">
								<label class="col-sm-2 control-label">Lastname</label>
								<div class="col-sm-10">
									<input type="text" class="form-control pull-right" id="lastname" placeholder="lastname">
								</div>
							</div>
							<br></br>
							<div class="form-group">
								<label class="col-sm-2 control-label">Credit card</label>
								<div class="col-sm-10">
									<input type="text" class="form-control pull-right" id="CardNo" placeholder="Last 4 Digits">
								</div>
							</div>
						</div>
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-info pull-right" onclick="searchReceipts()">Search Receipts</button>
					</div>
					<!-- /.box-footer -->
					
				</div>
			
			<div class="box box-default">
				<div class="box-header with-border">
					<h3 class="box-title">Receipt List</h3>
				</div>
				<div class="box-body" id="report_table" style="visibility:hidden">
					<div class="col-xs-12">
						<div class="span3">
							<div id="ReceiptList"><b>Report info will be listed here...</b></div>
						</div>
					</div>
				</div>
				
			</div>
			<!-- /.box -->
			
			</section>
			<!-- /.content -->
		</div>
		<!-- /.container -->
	</div>
	<!-- /.content-wrapper -->
	<footer class="main-footer">
		<div class="container">
			<div class="pull-right hidden-xs">
				<b>Version</b> 2.0
			</div>
			<strong>American Changer Reporter</strong> All rights reserved.
		</div>
		<!-- /.container -->
	</footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="js/LTE/app.min.js"></script>
<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- Page script -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.0.272/jspdf.debug.js"></script>
<script src="js/scripts.js"></script>
<script src="js/pdfGen.js"></script>
<script>
	$(function () {
		//$('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
		
		function cb(start, end) {
			$('#reportrange span').html(start.format('YYYY-MM-DD H:mm') + ' - ' + end.format('YYYY-MM-DD H:mm'));
		}
		
		cb(moment().startOf('days'), moment().endOf('days'));
		
		$('#reportrange').daterangepicker({
			timePicker: true,
			timePicker24Hour:true,
			timePickerIncrement: 1,
			
			ranges: {
			   'Today': [moment().startOf('days'), moment().endOf('days')],
			   'Yesterday': [moment().subtract(1, 'days').startOf('days'), moment().subtract(1, 'days').endOf('days')],
			   'Last 7 Days': [moment().subtract(6, 'days').startOf('days'), moment().endOf('days')],
			   'Last 30 Days': [moment().subtract(29, 'days').startOf('days'), moment().endOf('days')],
			   'This Month': [moment().startOf('month'), moment().endOf('month')],
			   'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			}
		}, cb);
	});
</script>
</body>
</html>
