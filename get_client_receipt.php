<?php
	
	session_start();
	$_SESSION['operator'] = "E.K. Fernandez Shows";
	ini_set("display_errors", true);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>American Changer Reporter</title>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="bootstrap/css/bootstrap.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
		<link rel="stylesheet" href="css/AdminLTE.css">
		<link rel="stylesheet" href="css/skins/_all-skins.min.css">
		<link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
		<script type="text/javascript" src="js/index2.js"></script>
		<script type="text/javascript" src="js/jquery.canvasjs.min.js"></script>
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->

<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">
	<header class="main-header">
		<nav class="navbar navbar-static-top">
			<div class="container">
				<div class="navbar-header">
					<a class="navbar-brand" href="index.php">
						<img alt="Brand" src="/AmericanChanger/img/amchanger40.png">
					</a>
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
						<i class="fa fa-bars"></i>
					</button>
				</div>
			</div>
		<!-- /.container-fluid -->
		</nav>
	</header>
  <!-- Full Width Column -->
	<div class="content-wrapper">
		<div class="container">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
				  Get Client Receipt
				  <!--<small>Example 2.0</small>-->
				</h1>
			</section>

			<!-- Main content -->
			<section class="content">
				<div class="box box-primary">
					<div class="box-header with-border">
					  <h3 class="box-title">Insert Client Lastname and credit card last 4 digits.</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					<div class="box-body">
						<div class="col-sm-12">
							<!-- Date and time range -->
							<div class="form-group">
								<label class="col-sm-2 control-label">Lastname</label>
								<div class="col-sm-10">
									<input type="text" class="form-control pull-right" id="lastname" placeholder="lastname" required>
								</div>
							</div>
							<br></br>
							<div class="form-group">
								<label class="col-sm-2 control-label">Credit card</label>
								<div class="col-sm-10">
									<input type="text" class="form-control pull-right" id="CardNo" placeholder="Last 4 Digits" required>
								</div>
							</div>
						</div>
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<button class="btn btn-error pull-left" onClick='location.href = "login.php"'>Back To Login</button>
						<button type="submit" class="btn btn-primary pull-right" onclick="searchReceipts()">Search Receipts</button>
					</div>
					<!-- /.box-footer -->
				</div>
			
			<div class="box box-default">
				<div class="box-header with-border">
					<h3 class="box-title">Receipt List</h3>
				</div>
				<div class="box-body" id="report_table" style="visibility:hidden">
					<div class="col-xs-12">
						<div class="span3">
							<div id="ReceiptList"><b>Report info will be listed here...</b></div>
						</div>
					</div>
				</div>
				
			</div>
			<!-- /.box -->
			
			</section>
			<!-- /.content -->
		</div>
		<!-- /.container -->
	</div>
	<!-- /.content-wrapper -->
	<footer class="main-footer">
		<div class="container">
			<div class="pull-right hidden-xs">
				<b>Version</b> 2.0
			</div>
			<strong>American Changer Reporter</strong> All rights reserved.
		</div>
		<!-- /.container -->
	</footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="js/LTE/app.min.js"></script>
<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- Page script -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.0.272/jspdf.debug.js"></script>
<script src="js/scripts.js"></script>
<script src="js/pdfGen.js"></script>
<script>
	$(function () {
		//$('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
		
		function cb(start, end) {
			$('#reportrange span').html(start.format('YYYY-MM-DD H:mm') + ' - ' + end.format('YYYY-MM-DD H:mm'));
		}
		
		cb(moment().startOf('days'), moment().endOf('days'));
		
		$('#reportrange').daterangepicker({
			timePicker: true,
			timePicker24Hour:true,
			timePickerIncrement: 1,
			
			ranges: {
			   'Today': [moment().startOf('days'), moment().endOf('days')],
			   'Yesterday': [moment().subtract(1, 'days').startOf('days'), moment().subtract(1, 'days').endOf('days')],
			   'Last 7 Days': [moment().subtract(6, 'days').startOf('days'), moment().endOf('days')],
			   'Last 30 Days': [moment().subtract(29, 'days').startOf('days'), moment().endOf('days')],
			   'This Month': [moment().startOf('month'), moment().endOf('month')],
			   'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			}
		}, cb);
	});
</script>
</body>
</html>
