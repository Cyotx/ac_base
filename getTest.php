<?php
  session_start();
  $itemsInCart = $_SESSION['itemsInCart'];
?>

<!DOCTYPE html>
<html>
<head>
  <!-- Site made with Mobirise Website Builder v4.8.1, https://mobirise.com -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Mobirise v4.8.1, mobirise.com">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
  <link rel="shortcut icon" href="assets/images/americanchangerlogo-384x124.png" type="image/x-icon">
  <meta name="description" content="Web Creator Description">
  <title>Index</title>
  <link rel="stylesheet" href="assets/tether/tether.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="assets/shopping-cart/minicart-theme.css">
  <link rel="stylesheet" href="assets/animatecss/animate.min.css">
  <link rel="stylesheet" href="assets/datatables/data-tables.bootstrap4.min.css">
  <link rel="stylesheet" href="assets/theme/css/style.css">
  <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">

  <!-- not generated assets -->
  <script type="text/javascript" src="assets/js/index2.js"></script>
  <script type="text/javascript" src="assets/js/ac2.js"></script>
  
  <script src="assets/web/assets/jquery/jquery.min.js"></script>
  <script src="assets/popper/popper.min.js"></script>
  <script src="assets/tether/tether.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/smoothscroll/smooth-scroll.js"></script>
  <script src="assets/dropdown/js/script.min.js"></script>
  <script src="assets/touchswipe/jquery.touch-swipe.min.js"></script>
  <script src="assets/viewportchecker/jquery.viewportchecker.js"></script>
  <script src="assets/datatables/jquery.data-tables.min.js"></script>
  <script src="assets/datatables/data-tables.bootstrap4.min.js"></script>
  
  <script src="assets/shopping-cart/minicart.js"></script>
  <script src="assets/shopping-cart/jquery.easing.min.js"></script>
  <script src="assets/shopping-cart/minicart-customizer.js"></script>
  <script src="assets/theme/js/script.js"></script>
</head>
<body>

  <?php
    ini_set("display_errors", false);
    $command = $_GET["command"]; 
    
    require  './db_config.php'; 

    $db_link = $link;

    if ($command == "searchReceipts"){
      //$q = intval($_GET['q']);

      
      $CardNo =  $_GET['CardNo'];
      $lastname = $_GET['lastname'];
      
      $sql = "call getClientReceipts(".$lastname.", ".$CardNo.");";
      //echo $sql;
      
      $result = mysqli_query($db_link,$sql);
      //echo $result;
      
      $list1 = "<div class='list-group' style=\"width:100%\">";


      //datetime
      while($row = mysqli_fetch_array($result)) {
        
        $list1 .= "<a style=\"cursor:pointer\" onclick=\"downloadPDF('".$row['ReceiptDataString']."', '".$row['ReceiptDateTime']."')\" 
        data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Click to Download Receipt Data \"
        class=\"list-group-item\" >Date and Time: ".$row['ReceiptDateTime']."</a>";
        
      }
      $list1 .= "</div>";
      
      echo "<h4>Receipt List</h4>";
      echo $list1;
      
    }
    
    if ($command == "loadKioskByOpName"){
      //$q = intval($_GET['q']);
      $q = $_GET['q'];
      
      $sql = "call getKioskByOperatorName('".$q."')";	
      $result = mysqli_query($db_link,$sql);

      echo "<table class=\"table table-bordered table-hover\">

      <tr>
      <th>IdKiosk</th>
      <th>Kiosk Name</th>
      <th>Serial Number</th>
      <th>Status</th>
      <th>Operator</th>
      </tr>";
      while($row = mysqli_fetch_array($result)) {
        echo "<tr>";
        echo "<td>" . $row['idKiosk'] . "</td>";
        echo "<td>" . $row['KioskName'] . "</td>";
        echo "<td>" . $row['SerialNumber'] . "</td>";
        echo "<td>" . $row['StatusDescription'] . "</td>";
        echo "<td>" . $row['OperatorName'] . "</td>";
        echo "</tr>";
      }
      echo "</table>";
    }
    
    if ($command == "loadAnalysis"){
      //$q = intval($_GET['q']);
      $ReportType = $_GET['Type'];
      $RTypeInt = intval($ReportType[1]);
      
      
      $Op = $_GET['Op'];
      $Kiosk = $_GET['Kiosk'];
      $PayType = $_GET['PayType'];
      $DateLow = $_GET['DateLow'];
      $DateHigh = $_GET['DateHigh'];
      $Months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
      $InitDate=explode("-", $DateLow);
      $EndDate=explode("-", $DateHigh);
      
      $SalesType = intval($_GET['SalesType'][1]);
      
      if ( $SalesType == 1){
        $Title = "<h3> Complete Sales </h3>";
      }
      if ( $SalesType == 2){
        $Title = "<h3> Incomplete Sales </h3>";
      }
      if ( $SalesType == 3){
        $Title = "<h3> All Sales </h3>";
      }
      echo $Title;
      
      $monthDiff=intval($EndDate[1]) - intval($InitDate[1]) + 1;
      $MontNumber = intval($InitDate[1])-1;
      $DayLimits = [31,29,31,30,31,30,31,31,30,31,30,31];
      if ($RTypeInt == 1){
        $Search = "Hour range";
        
        $RLimit = 24;
        $Headers = ['00:00 - 00:59','01:00 - 01:59', '02:00 - 02:59', '03:00 - 03:59','04:00 - 04:59', '05:00 - 05:59',
              '06:00 - 06:59','07:00 - 07:59', '08:00 - 08:59', '09:00 - 09:59','10:00 - 10:59', '11:00 - 11:59',
              '12:00 - 12:59','13:00 - 13:59','14:00 - 14:59', '15:00 - 15:59', '16:00 - 16:59','17:00 - 17:59', 
              '18:00 - 18:59','19:00 - 19:59','20:00 - 20:59', '21:00 - 21:59', '22:00 - 22:59','23:00 - 23:59'];
        //"ByDaysHour";
        $Lidx = -1;
      }
      if ($RTypeInt == 2){
        $Search = "Day of Month";
        $RLimit = 0;
        
        $Dates = ['1st','2nd','3rd','4th','5th','6th','7th','8th','9th','10th','11th','12th','13th','14th','15th','16th',
              '17th','18th','19th','20th','21st','22nd','23rd','24th','25th','26th','27th','28th','29th','30th','31st'];
              
        $Headers = array();
        $y = 0;
        $Month = $Months[$MontNumber];
        while ($y < $monthDiff){
          
          for ($x = 0; $x < $DayLimits[$MontNumber+$y]; $x++) {
            
            array_push($Headers, $Month ." ". $Dates[$x]);
          }
          $RLimit+=$DayLimits[$MontNumber+$y];
          $y ++;
          $Month = $Months[$MontNumber+$y];
        }
        $Lidx = 0;
        //"ByDaysOfMonth";
      }
      if ($RTypeInt == 3){
        $Search = "Weekday";
        $RLimit = 7;
        $Headers = ['Sunday','Monday','Tuesday','Wednesday','Thurday','Friday','Saturday'];
        //"ByWeekDay";
        $Lidx = 0;
      }
      if ($RTypeInt == 4){
        $Search = "Month";
        $RLimit = 12;
        $Headers = ['January','February','March','April','May','June','July','August','September','October','November','December'];
        //"ByMonth";
        $Lidx = 0;
      }
      
        
      $MontNumber = intval($InitDate[1]);
      
      $Lines = array();
      for ($x = 0; $x < $RLimit; $x++) {
        if ($x == $DayLimits[$MontNumber]){
          $MontNumber++;
        }
        
        $Lidx = $Lidx+1;
        $Line = [$Headers[$x]];					
        // Package
        $sql = "call getPackageDataByReportTypeRework(".$Op.", ".$Kiosk.", ".$PayType.",".$SalesType.",".$DateLow.", ".$DateHigh.",".$ReportType.",".$Lidx.",'".$MontNumber."')";
        //echo $sql;
        //$sql = "call getPackageDataByReportType(".$Op.", ".$Kiosk.", ".$PayType.",".$DateLow.", ".$DateHigh.",".$ReportType.",".$Lidx.",'7')";
        //$sql = "call getPackageDataByReportType('Test Operator', 'T7','3','2016-03-06','2016-07-26','1', '18','7')";
        
        $db_link1 = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        $result = mysqli_query($db_link1,$sql);
        while($Pack = mysqli_fetch_array($result)) {
          if ($Pack["SUM(PackQuantity)"] == "")
            $Pack["SUM(PackQuantity)"] = 0;
          if ($Pack["SUM(PackTotalPurchase)"] == "")
            $Pack["SUM(PackTotalPurchase)"] = 0;
          array_push($Line, $Pack["SUM(PackQuantity)"]);
          array_push($Line, $Pack["SUM(PackTotalPurchase)"]);
        }
        mysqli_close($db_link1);
        // wristband
        $sql = "call getWristbandDataByReportType(".$Op.", ".$Kiosk.", ".$PayType.",".$DateLow.", ".$DateHigh.",".$ReportType.",".$Lidx.",".$MontNumber.")";				
        //$sql = "call getWristbandDataByReportType('Test Operator', 'T7','3','2016-03-06','2016-07-26','1', '18','7')";
        //echo $sql;
        $db_link1 = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        
        $result = mysqli_query($db_link1,$sql);
        while($WB = mysqli_fetch_array($result)) {
          if ($WB["SUM(WBQuantity)"] == "")
            $WB["SUM(WBQuantity)"] = 0;
          if ($WB["SUM(WBTotalCash)"] == "")
            $WB["SUM(WBTotalCash)"] = 0;
          array_push($Line, $WB["SUM(WBQuantity)"]);
          array_push($Line, $WB["SUM(WBTotalCash)"]);
        }
        array_push($Line, $Line[12] + $Line[18]);
        array_push($Lines, $Line);
        mysqli_close($db_link1);
      }

      // All sales
      echo "
      <table class=\"table table-bordered table-hover\">
      <tr>
        <th rowspan=\"2\">".$Search."</th>
        <th colspan=\"2\">Package 1</th>
        <th colspan=\"2\">Package 2</th>
        <th colspan=\"2\">Package 3</th>
        <th colspan=\"2\">Package 4</th>
        <th colspan=\"2\">Package 5</th>
        <th colspan=\"2\">All Packages</th>
        <th colspan=\"2\">Wristband Adult</th>
        <th colspan=\"2\">Wristband Child</th>
        <th colspan=\"2\">All Wristbands</th>
        <th rowspan=\"2\">All Sales</th>
      </tr>
      <tr>
        <th>Qty</th><th>Gross Sales</th>
        <th>Qty</th><th>Gross Sales</th>
        <th>Qty</th><th>Gross Sales</th>
        <th>Qty</th><th>Gross Sales</th>
        <th>Qty</th><th>Gross Sales</th>
        <th>Qty</th><th>Gross Sales</th>
        <th>Qty</th><th>Gross Sales</th>
        <th>Qty</th><th>Gross Sales</th>
        <th>Qty</th><th>Gross Sales</th>
      </tr>";
      // <td> ".$AllSales[$x]." </td>
      for ($x = 0; $x < $RLimit; $x++) {
        echo "<tr>
        <td>".$Lines[$x][0]."</td>
        <td>".$Lines[$x][1]."</td><td>".$Lines[$x][2]."</td>
        <td>".$Lines[$x][3]."</td><td>".$Lines[$x][4]."</td>
        <td>".$Lines[$x][5]."</td><td>".$Lines[$x][6]."</td>
        <td>".$Lines[$x][7]."</td><td>".$Lines[$x][8]."</td>
        <td>".$Lines[$x][9]."</td><td>".$Lines[$x][10]."</td>
        <td>".$Lines[$x][11]."</td><td>".$Lines[$x][12]."</td>
        <td>".$Lines[$x][13]."</td><td>".$Lines[$x][14]."</td>
        <td>".$Lines[$x][15]."</td><td>".$Lines[$x][16]."</td>
        <td>".$Lines[$x][17]."</td><td>".$Lines[$x][18]."</td>
        <td>".$Lines[$x][19]."</td>
        
        </tr>";
      }
      // Cargar graficas personalizadas 31 la mas larga /All Sales:".$AllSales[0]."/".$AllSales[2]."/".$AllSales[1]."
      echo "<div id=\"div1\" style=\"visibility:hidden\">";
      for ($x = 0; $x < $RLimit; $x++) {
        echo "/".$Lines[$x][0]."!".$Lines[$x][12]."/".$Lines[$x][18]."*";
      }
      echo "</div>";
    }
    
    if ($command == "loadTransactions"){
      //$q = intval($_GET['q']);
      $Op = $_GET['Op'];
      $Kiosk = (string)$_GET['Kiosk'];
      $PayType = $_GET['PayType'];
      $Qty = $_GET['Qty'];
      $DateLow = $_GET['DateLow'];
      $DateHigh = $_GET['DateHigh'];
      
      $sql0 = "call getReportType(".$Op.");";
      $db_link0 = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
      $result0 = mysqli_query($db_link0,$sql0);
      if ($row0 = mysqli_fetch_array($result0)){
        $repType = $row0['ReportType'];
      }
      mysqli_close($db_link0);
      
      $sql = "call getLastTransactionsRework(".$Op.", ".$Kiosk.", ".$Qty.", ".$DateLow.", ".$DateHigh.",".$PayType.")";	
      //echo $sql;

      $db_link1 = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
      
      $result = mysqli_query($db_link1,$sql);

      //echo $Kiosk;
      echo "<table class=\"table table-bordered table-hover\">
      <tr>";

      /*if ($Kiosk == "All Kiosks"){
        echo "<th>Kiosk Name</th>";
      }*/
      echo "<th>Kiosk Name</th>";
      
      switch ($repType) {
        case 0:
        case 1:
        case 3:
          echo "<th>Card Serial Number</th>";
          break;
        case 2:
          echo "<th>Wristband Code(s)</th>";
          break;
        case 4:
          echo "<th>Transaction Number</th>";
          break;
      }
      echo "<th>Date and Time</th>
      <th>Concept</th>
      <th>Quantity</th>
      <th>Purchase Amount ($)</th>
      <th>Payment Information</th>";
      switch ($repType) {
        case 0:
        case 1:
        case 3:
          echo "<th>Credits Added</th>";
          break;
        default:
          break;
      }
      echo "</tr>";
      
      while($row = mysqli_fetch_array($result)) {
        echo "<tr>";
        /*if ($Kiosk == "All Kiosks"){
          echo "<td>" . $row['KioskName'] . "</td>";
        }*/
        echo "<td>" . $row['KioskName'] . "</td>";
        echo "<td>" . $row['transactionId'] . "</td>";
        echo "<td>" . $row['xDate'] . "</td>";
        echo "<td>" . $row['Concept'] . "</td>";
        echo "<td>" . $row['Quantity'] . "</td>";
        echo "<td>" . $row['PurchaseAmount'] . "</td>";
        echo "<td>" . $row['PaymentTypeDescription'] . "</td>";
        /*if (($repType != 2) && ($repType != 4)) {
          echo "<td>" . $row['Credits'] . "</td>";
        }*/
        echo "</tr>";
      }
      echo "</table>";
      mysqli_close($db_link1);
    }
    
    if ($command == "searchCards"){
      $Op = $_GET['Op'];
      $Search = $_GET['Search'];
      
      $sql0 = "call getReportType(".$Op.");";
      $db_link0 = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
      $result0 = mysqli_query($db_link0,$sql0);
      if ($row0 = mysqli_fetch_array($result0)){
        $repType = $row0['ReportType'];
      }
      mysqli_close($db_link0);
      
      if ($repType != 2){
        $sql = "call searchCardHistory(".$Search.",".$Op.",'1')";
        //$sql = "call searchCardHistory('48884287', 'E.K. Fernandez Shows')";
        //$sql = "call searchTransaction(".$Op.", ".$Kiosk.", '92')";
        $db_link1 = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        
        $result = mysqli_query($db_link1,$sql);
        echo "<h3>Card Number: ".$Search."</h3>";
        echo "<div class=\"col-xs-6\">";
        echo "<h4>Complete Sales</h4>";
        echo "<table class=\"table table-bordered table-hover\">
        <tr>
        <th>Kiosk</th>
        <th>Card Number</th>
        <th>Starting Balance</th>
        <th>Ending Balance</th>
        <th>Transaction Amount</th>
        <th>Date and Time</th>
        </tr>";
        
        while($row = mysqli_fetch_array($result)) {
          echo "<tr>";
          echo "<td>" . $row['KioskName'] . "</td>";
          echo "<td>" . $row['CardNumber'] . "</td>";
          echo "<td>" . $row['StartingBalance'] . "</td>";
          echo "<td>" . $row['EndingBalance'] . "</td>";
          echo "<td>" . $row['Amount'] . "</td>";
          echo "<td>" . $row['Time'] . "</td>";
          echo "</tr>";
        }
        echo "</table>";
        echo "</div>";
        mysqli_close($db_link1);
        // INCOMPLETE SALES
        
        $sql = "call searchCardHistory(".$Search.",".$Op.",'2')";
        //$sql = "call searchCardHistory('48884287', 'E.K. Fernandez Shows')";
        //$sql = "call searchTransaction(".$Op.", ".$Kiosk.", '92')";
        $db_link1 = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        
        $result = mysqli_query($db_link1,$sql);
        echo "<div class=\"col-xs-offset-1 col-xs-5\">";
        echo "<h4>Incomplete Sales</h4>";
        echo "<table class=\"table table-bordered table-hover\">
        <tr>
        <th>Kiosk</th>
        <th>Card Number</th>
        <th>Starting Balance</th>
        <th>Ending Balance</th>
        <th>Transaction Amount</th>
        <th>Date and Time</th>
        </tr>";
        
        while($row = mysqli_fetch_array($result)) {
          echo "<tr>";
          echo "<td>" . $row['KioskName'] . "</td>";
          echo "<td>" . $row['CardNumber'] . "</td>";
          echo "<td>" . $row['StartingBalance'] . "</td>";
          echo "<td>" . $row['EndingBalance'] . "</td>";
          echo "<td>" . $row['Amount'] . "</td>";
          echo "<td>" . $row['Time'] . "</td>";
          echo "</tr>";
        }
        echo "</table>";
        echo "</div>";
        mysqli_close($db_link1);
      }
      else{
        echo "<h3>The operator ".$Op." does not have card search Feature</h3>";
      }
    }
    
    if ($command == "searchTransactions"){
      //$q = intval($_GET['q']);
      $Op = $_GET['Op'];
      $Kiosk = $_GET['Kiosk'];
      $Search = $_GET['Search'];
      
      $sql0 = "call getReportType(".$Op.");";
      $db_link0 = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
      $result0 = mysqli_query($db_link0,$sql0);
      if ($row0 = mysqli_fetch_array($result0)){
        $repType = $row0['ReportType'];
      }
      mysqli_close($db_link0);

      $sql = "call searchTransactionsRework(".$Op.", ".$Kiosk.", ".$Search.", ".$repType.")";
      
      //$sql = "call searchTransaction(".$Op.", ".$Kiosk.", '92')";
      $db_link1 = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
      
      $result = mysqli_query($db_link1,$sql);

      echo "<table class=\"table table-bordered table-hover\">
      <tr>";
      
      echo "<th>Machine Name</th>";
      if ($repType == 2){
        echo "<th>Wristband Code(s)</th>";
      }
      if ($repType == 1){
        echo "<th>Card Serial Number</th>";
      }
      //else{
      //	echo "<th>Wristband Code(s)/Card Serial Number</th>";
      //}
      echo "<th>Date and Time</th>
      <th>Concept</th>
      <th>Quantity</th>
      <th>Purchase Amount</th>
      <th>Payment Information</th>";
      if ($repType != 2){
        echo "<th>Credits Added</th>";
      }
      echo "</tr>";
      
      while($row = mysqli_fetch_array($result)) {
        echo "<tr>";
        echo "<td>" . $row['KioskName'] . "</td>";
        echo "<td>" . $row['transactionId'] . "</td>";
        echo "<td>" . $row['xDate'] . "</td>";
        echo "<td>" . $row['Concept'] . "</td>";
        echo "<td>" . $row['Quantity'] . "</td>";
        echo "<td>" . $row['PurchaseAmount'] . "</td>";
        echo "<td>" . $row['PaymentTypeDescription'] . "</td>";
        if ($repType != 2){
          echo "<td>" . $row['Credits'] . "</td>";
        }
        echo "</tr>";
      }
      echo "</table>";
      mysqli_close($db_link1);
    }
    
    if ($command == "searchTransactionsByClient"){
      //$q = intval($_GET['q']);
      
      $Op = $_GET['Op'];
      $Kiosk = $_GET['Kiosk'];
      $Lastname = $_GET['Lastname'];
      $Creditcard = $_GET['Creditcard'];
      
      /*
        * Leaving this in case the report type becomes a necessity
      $sql0 = "call getReportType(".$Op.");";
      $db_link0 = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
      $result0 = mysqli_query($db_link0,$sql0);
      if ($row0 = mysqli_fetch_array($result0)){
        $repType = $row0['ReportType'];
      }
      mysqli_close($db_link0);
      */
      
      $sql = "call searchTransactionsByClient(".$Op.", ".$Kiosk.", ".$Lastname.", ".$Creditcard.")";

      $db_link1 = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
      
      $result = mysqli_query($db_link1,$sql);

      echo "<table class=\"table table-bordered table-hover\">
      <tr>";
      
      echo "<th>Machine Name</th>";
      echo "<th>Costumer Name</th>";
      echo "<th>Card Serial Number</th>";
      /*if ($repType == 2){
        echo "<th>Wristband Code(s)</th>";
      }
      if ($repType == 1){
        echo "<th>Card Serial Number</th>";
      }*/
      //else{
      //	echo "<th>Wristband Code(s)/Card Serial Number</th>";
      //}
      echo "<th>Date and Time</th>
      <th>Concept</th>
      <th>Quantity</th>
      <th>Purchase Amount</th>
      <th>Payment Information</th>";
      //if ($repType != 2){
        echo "<th>Credits Added</th>";
      //}
      echo "</tr>";
      
      while($row = mysqli_fetch_array($result)) {
        echo "<tr>";
        echo "<td>" . $row['KioskName'] . "</td>";
        echo "<td>" . $row['CreditCardHolder'] . "</td>";
        echo "<td>" . $row['transactionId'] . "</td>";
        echo "<td>" . $row['xDate'] . "</td>";
        echo "<td>" . $row['Concept'] . "</td>";
        echo "<td>" . $row['Quantity'] . "</td>";
        echo "<td>" . $row['PurchaseAmount'] . "</td>";
        echo "<td>" . $row['PaymentTypeDescription'] . "</td>";
        //if ($repType != 2){
          echo "<td>" . $row['Credits'] . "</td>";
        //}
        echo "</tr>";
      }
      echo "</table>";
      mysqli_close($db_link1);
    }
    
    if ($command == "loadPackageData"){
      
      $Op = $_GET['Op'];
      $Kiosk = $_GET['Kiosk'];
      $SalesType = intval($_GET['SalesType'][1]);
      $DateLow = $_GET['DateLow'];
      $DateHigh = $_GET['DateHigh'];
      
      if ( $SalesType == 1){
        $Title = "<h3> Complete Sales </h3>";
      }
      if ( $SalesType == 2){
        $Title = "<h3> Incomplete Sales </h3>";
      }
      if ( $SalesType == 3){
        $Title = "<h3> All Sales </h3>";
      }
      echo $Title;
        
      $Lines = array();
      $Headers = ['Cash','Credit Card', 'Total'];
      for ($x = 0; $x < 3; $x++) {
        $PayType = $x+1;
        
        $Line = [$Headers[$x]];
        $sql = "call getPackageDataByPaymentTypeRework(".$Op.", ".$Kiosk.", ".$PayType.",".$SalesType.",".$DateLow.", ".$DateHigh.")";
        //echo $sql;
        $db_link1 = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        
        $result = mysqli_query($db_link1,$sql);
        while($Pack = mysqli_fetch_array($result)) {
          if ($Pack["SUM(PackQuantity)"] == "")
            $Pack["SUM(PackQuantity)"] = 0;
          if ($Pack["SUM(PackTotalPurchase)"] == "")
            $Pack["SUM(PackTotalPurchase)"] = 0;
          array_push($Line, $Pack["SUM(PackQuantity)"]);
          array_push($Line, $Pack["SUM(PackTotalPurchase)"]);
        }
        mysqli_close($db_link1);
        // agregar cosa de wristband
        
        $sql = "call getWristbandDataByPaymentType(".$Op.", ".$Kiosk.", ".$PayType.",".$DateLow.", ".$DateHigh.")";	
        //echo $sql;
        $db_link1 = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        
        $result = mysqli_query($db_link1,$sql);
        while($WB = mysqli_fetch_array($result)) {
          if ($WB["SUM(WBQuantity)"] == "")
            $WB["SUM(WBQuantity)"] = 0;
          if ($WB["SUM(WBTotalCash)"] == "")
            $WB["SUM(WBTotalCash)"] = 0;
          array_push($Line, $WB["SUM(WBQuantity)"]);
          array_push($Line, $WB["SUM(WBTotalCash)"]);
        }
        mysqli_close($db_link1);
        array_push($Line, $Line[12] + $Line[18]);
        array_push($Lines, $Line);
      }

      // All sales
      
      echo "
      <table class=\"table table-bordered table-hover\">
      <tr>
        <th rowspan=\"2\">Sale Type</th>
        <th colspan=\"2\">Package 1</th>
        <th colspan=\"2\">Package 2</th>
        <th colspan=\"2\">Package 3</th>
        <th colspan=\"2\">Package 4</th>
        <th colspan=\"2\">Package 5</th>
        <th colspan=\"2\">All Packages</th>
        <th colspan=\"2\">Wristband Adult</th>
        <th colspan=\"2\">Wristband Child</th>
        <th colspan=\"2\">All Wristbands</th>
        <th rowspan=\"2\">All Sales</th>
      </tr>
      <tr>
        <th>Qty</th><th>Gross Sales</th>
        <th>Qty</th><th>Gross Sales</th>
        <th>Qty</th><th>Gross Sales</th>
        <th>Qty</th><th>Gross Sales</th>
        <th>Qty</th><th>Gross Sales</th>
        <th>Qty</th><th>Gross Sales</th>
        <th>Qty</th><th>Gross Sales</th>
        <th>Qty</th><th>Gross Sales</th>
        <th>Qty</th><th>Gross Sales</th>
      </tr>";
      
      for ($x = 0; $x < 3; $x++) {
        echo "<tr>
        <td>".$Lines[$x][0]."</td>
        <td>".$Lines[$x][1]."</td><td>".$Lines[$x][2]."</td>
        <td>".$Lines[$x][3]."</td><td>".$Lines[$x][4]."</td>
        <td>".$Lines[$x][5]."</td><td>".$Lines[$x][6]."</td>
        <td>".$Lines[$x][7]."</td><td>".$Lines[$x][8]."</td>
        <td>".$Lines[$x][9]."</td><td>".$Lines[$x][10]."</td>
        <td>".$Lines[$x][11]."</td><td>".$Lines[$x][12]."</td>
        <td>".$Lines[$x][13]."</td><td>".$Lines[$x][14]."</td>
        <td>".$Lines[$x][15]."</td><td>".$Lines[$x][16]."</td>
        <td>".$Lines[$x][17]."</td><td>".$Lines[$x][18]."</td>
        <td> ".$Lines[$x][19]." </td>
        </tr>";
      }

      
      echo "<div id=\"div1\" style=\"visibility:hidden\">
      /Pkg Cash:".$Lines[0][11]."/".$Lines[0][12]."-
      /Pkg Credit:".$Lines[1][11]."/".$Lines[1][12]."-
      /All Pkgs:".$Lines[2][11]."/".$Lines[2][12]."-
      /WB Cash:".$Lines[0][17]."/".$Lines[0][18]."-
      /WB Credit:".$Lines[1][17]."/".$Lines[1][18]."-		
      /All WBs:".$Lines[2][17]."/".$Lines[2][18]."-
      /All Sales:".$Lines[0][19]."/".$Lines[2][19]."/".$Lines[1][19]."
      </div>";
    }
    
    if ($command == "loadTotalKiosk"){
      $Op = $_GET['Op'];
      $Kiosk = $_GET['Kiosk'];
      $DateLow = $_GET['DateLow'];
      $DateHigh = $_GET['DateHigh'];
      
      $SalesToReport = $_GET['SalesToReport'];
      $SalesToReport = intval($SalesToReport[1]);
      
      $qty = 1;
      $Line = array();
      //echo $SalesToReport;
      if ( $SalesToReport != 2 ){
        echo "<div class=\"col-xs-12\">
          <h3> Complete Sales</h3>
          <table class=\"table table-bordered table-hover\">
          <tr>
            <th>Machine Name</th>
            <th>Cash</th>
            <th>Credit Card</th>
            <th>Total</th>
            <th>Hard Meter</th>
          </tr>";
        
        // Complete sales
        if ($Kiosk == "'All Kiosks'"){
          // call once for each row
          $sql0 = "CALL getKioskQty(".$Op.")";
          //  
          $TotalCash   = 0;
          $TotalCredit = 0;
          $TotalGlobal = 0;
          
          $result = mysqli_query($db_link,$sql0);
          while ($row = mysqli_fetch_array($result)){
            array_push($Line, $row['KioskName']);
          }
          $count = count($Line);
          $i = 0;
          // get opId, get kiosk id before 0.235 each consult
          while ($count > $i){
            $db_link1 = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
            $sql = "call getAllSalesByKioskRework(".$Op.",'".$Line[$i]."',".$DateLow.", ".$DateHigh.",'1');";
            //echo $sql;
            $result2 = mysqli_query($db_link1,$sql);
            
            if($row = mysqli_fetch_array($result2)) {
              if ($row['Cash'] == "") $row['Cash'] = "0";
              if ($row['Credit'] == "") $row['Credit'] = "0";
              if ($row['Total'] == "") $row['Total'] = "0";
              $TotalCash   += intval($row['Cash']);
              $TotalCredit += intval($row['Credit']);
              $TotalGlobal += intval($row['Total']);
              echo "<tr>";
              echo "<td>".$row['kName']."</td>
                  <td>$".floatval($row['Cash'])."</td>
                  <td>$".floatval($row['Credit'])."</td>
                  <td>$".floatval($row['Total'])."</td>
                  <td>".floatval($row['HardMeter'])."</td>";
              echo "</tr>";
            }
            mysqli_close($db_link1);
            $i++;
          }
          echo "<tr>";
          echo "<td>Total</td><td>$".$TotalCash."</td>
              <td>$".$TotalCredit."</td><td>$".$TotalGlobal."</td>
              <td>---</td>";
          echo "</tr>";

        }

        else{
          //$sql = "call getAllSalesByKiosk(".$Kiosk.",'2016-05-27' , '2016-05-27')";
          $sql = "call getAllSalesByKioskRework(".$Op.",".$Kiosk.",".$DateLow.", ".$DateHigh.",'1');";
          //echo $sql;
          $result = mysqli_query($db_link,$sql);
        
          if($row = mysqli_fetch_array($result)) {
            echo "<tr>";
            echo "<td>".$row['kName']."</td>
                <td>$".round(floatval($row['Cash']), 2)."</td>
                <td>$".floatval($row['Credit'])."</td>
                <td>$".round(floatval($row['Total']), 2)."</td>
                <td>".round(floatval($row['HardMeter']), 2)."</td>";
            echo "</tr>";
          }
        }
        echo "</table></div>";
      }

      if ( $SalesToReport != 1 ){
        $incomplete=1;
        // INCOMPLETE SALES
        echo "<div class=\"col-xs-12\">
          <h3> Incomplete Sales </h3>
          <table class=\"table table-bordered table-hover\">
          <tr>
            <th>Machine Name</th>
            <th>Cash</th>
            <th>Credit Card</th>
            <th>Total</th>
            <th>Hard Meter</th>
          </tr>";
          
          /**/
        
        if ($Kiosk == "'All Kiosks'" && $incomplete==1){
          // call once for each row
          $Line = array();
          $sql0 = "CALL getKioskQty(".$Op.")";
          $db_link = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
          $result = mysqli_query($db_link,$sql0);
          
          $TotalCash   = 0;
          $TotalCredit = 0;
          $TotalGlobal = 0;
          
          while ($row = mysqli_fetch_array($result)){
            array_push($Line, $row['KioskName']);
          }
          $count = count($Line);
          $i = 0;
          while ($count > $i){
            $db_link1 = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
            $sql = "call getAllSalesByKioskRework(".$Op.",'".$Line[$i]."',".$DateLow.", ".$DateHigh.",'2');";
            $result2 = mysqli_query($db_link1,$sql);
            
            if($row = mysqli_fetch_array($result2)) {
              if ($row['Cash'] == "") $row['Cash'] = "0";
              if ($row['Credit'] == "") $row['Credit'] = "0";
              if ($row['Total'] == "") $row['Total'] = "0";
              $TotalCash   += intval($row['Cash']);
              $TotalCredit += intval($row['Credit']);
              $TotalGlobal += intval($row['Total']);
              echo "<tr>";
              echo "<td>".$row['kName']."</td>
                  <td>$".floatval($row['Cash'])."</td>
                  <td>$".floatval($row['Credit'])."</td>
                  <td>$".floatval($row['Total'])."</td>
                  <td>".floatval($row['HardMeter'])."</td>";
              echo "</tr>";
            }
            mysqli_close($db_link1);
            $i++;
          }
          echo "<tr>";
          echo "<td>Total</td><td>$".$TotalCash."</td>
              <td>$".$TotalCredit."</td><td>$".$TotalGlobal."</td>
              <td>---</td>";
          echo "</tr>";
        }
        else{
          //$sql = "call getAllSalesByKiosk(".$Kiosk.",'2016-05-27' , '2016-05-27')";
          $sql = "call getAllSalesByKioskRework(".$Op.",".$Kiosk.",".$DateLow.", ".$DateHigh.",'2');";
          $db_link = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
          $result = mysqli_query($db_link,$sql);
        
          
          if($row = mysqli_fetch_array($result)) {
            echo "<tr>";
            echo "<td>".$row['kName']."</td>
                <td>$".floatval($row['Cash'])."</td>
                <td>$".floatval($row['Credit'])."</td>
                <td>$".floatval($row['Total'])."</td>
                <td>".floatval($row['HardMeter'])."</td>";
            echo "</tr>";
          }
        }
        echo "</table></div>";
      }
    }
    
    if ($command == "loadTotalPkg"){
      //$q = intval($_GET['q']);
      $Op = $_GET['Op'];
      $Kiosk = $_GET['Kiosk'];
      $DateLow = $_GET['DateLow'];
      $DateHigh = $_GET['DateHigh'];
      
      $SalesType = intval($_GET['SalesType'][1]);
      $DateLow = $_GET['DateLow'];
      $DateHigh = $_GET['DateHigh'];
      
      $TotalP1 = 0;
      $TotalP2 = 0;
      $TotalP3 = 0;
      $TotalP4 = 0;
      $TotalP5 = 0;
      $TotalGlobal = 0;
      
      
      if ( $SalesType == 1){
        $Title = "<h3> Complete Sales </h3>";
      }
      if ( $SalesType == 2){
        $Title = "<h3> Incomplete Sales </h3>";
      }
      if ( $SalesType == 3){
        $Title = "<h3> All Sales </h3>";
      }
      echo $Title;
      
      $qty = 1;
      $Line = array();
      echo "
        <table class=\"table table-bordered table-hover\">
        <tr>
          <th>Machine</th>
          <th>Package #1</th>
          <th>Package #2</th>
          <th>Package #3</th>
          <th>Package #4</th>
          <th>Package #5</th>
          <th> Total</th>
        </tr>";
      
      if ($Kiosk == "'All Kiosks'"){
        // call once for each row
        $sql0 = "CALL getKioskQty(".$Op.")";
        //  
        
        $result = mysqli_query($db_link,$sql0);
        while ($row = mysqli_fetch_array($result)){
          array_push($Line, $row['KioskName']);
        }
        $count = count($Line);
        $i = 0;
        while ($count > $i){
          $db_link1 = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
          //$sql = "call getAllSalesByKiosk(".$Op.",'".$Line[$i]."',".$DateLow.", ".$DateHigh.");";
          $sql = "call getPackageTotalJoinRework(".$Op.",'".$Line[$i]."',".$SalesType.",".$DateLow.", ".$DateHigh.");";
          $result2 = mysqli_query($db_link1,$sql);
          
          if($row = mysqli_fetch_array($result2)) {
            if ($row['Package1'] == "") $row['Package1'] = "0";
            if ($row['Package2'] == "") $row['Package2'] = "0";
            if ($row['Package3'] == "") $row['Package3'] = "0";
            if ($row['Package4'] == "") $row['Package4'] = "0";
            if ($row['Package5'] == "") $row['Package5'] = "0";
            if ($row['Total'] == "") $row['Total'] = "0";
            
            $TotalP1 += intval($row['Package1']);
            $TotalP2 += intval($row['Package2']);
            $TotalP3 += intval($row['Package3']);
            $TotalP4 += intval($row['Package4']);
            $TotalP5 += intval($row['Package5']);
            $TotalGlobal += intval($row['Total']);
            
            echo "<tr>";
            echo "<td>".$row['KioskName']."</td><td>$".$row['Package1']."</td>
                <td>$".$row['Package2']."</td><td>$".$row['Package3']."</td>
                <td>$".$row['Package4']."</td><td>$".$row['Package5']."</td>
                <td>$".$row['Total']."</td>";
            echo "</tr>";
          }
          mysqli_close($db_link1);
          $i++;
        }
        echo "<tr>";
        echo "<td>Total</td>
          <td>$".$TotalP1."</td>
          <td>$".$TotalP2."</td>
          <td>$".$TotalP3."</td>
          <td>$".$TotalP4."</td>
          <td>$".$TotalP5."</td>
          <td>$".$TotalGlobal."</td>";
        echo "</tr>";
      }
      else{
        $sql = "call getPackageTotalJoinRework(".$Op.",".$Kiosk.",".$SalesType.",".$DateLow.", ".$DateHigh.");";
        $result = mysqli_query($db_link,$sql);
      
        
        if($row = mysqli_fetch_array($result)) {
          if ($row['Package1'] == "") $row['Package1'] = "-";
          if ($row['Package2'] == "") $row['Package2'] = "-";
          if ($row['Package3'] == "") $row['Package3'] = "-";
          if ($row['Package4'] == "") $row['Package4'] = "-";
          if ($row['Package5'] == "") $row['Package5'] = "-";
          if ($row['Total'] == "") $row['Total'] = "-";
          echo "<tr>";
          echo "<td>".$row['KioskName']."</td><td>$".$row['Package1']."</td>
              <td>$".$row['Package2']."</td><td>$".$row['Package3']."</td>
              <td>$".$row['Package4']."</td><td>$".$row['Package5']."</td>
              <td>$".$row['Total']."</td>";
          echo "</tr>";
        }
      }
    }
    
    if ($command == "autoFillInfo"){
      //$q = intval($_GET['q']);
      $q = $_GET['q'];
      $Page = $_GET['page'];
      
      $sql = "call getKioskByOperatorName('".$q."')";
      
      $result = mysqli_query($db_link,$sql);
      
      $select1="";
      $select2="";
      
      $select1 .=  "<select class=\"form-control\" Id=\"k_name\">
        <option value=\"\">Kiosk Name</option>";
      
      if ($Page == 'totalKiosk' || $Page == 'totalPkg' || $Page == 'transaction'){
        $select1 .= "<option value='All Kiosks'>All Kiosks</option>";
        
      }
      /*$select2 .= "<select class=\"col-xs-offset-1 col-xs-4\" Id=\"k_number\">
        <option value=\"\">Kiosk Serial Number</option>";*/
        
      while($row = mysqli_fetch_array($result)) {
        $select1 .= "<option value='" . $row['KioskName'] ." '>" . $row['KioskName'] . "</option>";
        $select2 .= "<option value=\"" . $row['SerialNumber'] ." \">" . $row['SerialNumber'] . "</option>";
      }
      $select1 .=  "</select>";	
      $select2 .= "</select>";	
      
      echo $select1;
      //echo $select2;	
    }
    
    if ($command == "fillInfoOperations"){
      $q = $_GET['q'];
            
      $sql = "call getKioskByOperatorName('".$q."')";
      
      $result = mysqli_query($db_link,$sql);
      
      $select1="";
      $select2="";

        
      $list1 = "<div class=\"col-xs-12\"><div class='list-group'>";
      
      $db_link2 = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
      $sql2 = "call getOperatorData('".$q."')";
      //echo $sql2;
      $result2 = mysqli_query($db_link2,$sql2);
      if ($row2 = mysqli_fetch_array($result2)){
        while($row = mysqli_fetch_array($result)) {
          $db_link3 = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
          $sql3 = "call getMachineStatus('".$row['idKiosk']."')";
          $result3 = mysqli_query($db_link3,$sql3);
          if ($row3 = mysqli_fetch_array($result3)){
            
            $list1 .= "<a onclick=\"showDetailsPanel('".$row['KioskName']."','".$row['KioskIpAddress']."','".$row['SerialNumber']."',
            '".$row['kioskNotes']."','".$row['idKiosk']."', '".$row2['idOperator']."','".$row2['FirstName']."','".$row2['LastName']."',
            '".$row2['Email']."','".$row2['loginUsername']."','".$row2['PhoneNumber']."',
            '".$row3['SoftwareVersion']."','".$row3['System']."','".$row3['CarWash']."','".$row3['CardDispenser1']."','".$row3['CardDispenser2']."',
            '".$row3['CardDispenser3']."','".$row3['CardDispenser4']."','".$row3['TicketDispenser1']."','".$row3['TicketDispenser2']."','".$row3['TicketDispenser3']."',
            '".$row3['TicketDispenser4']."','".$row3['Hopper1']."','".$row3['Hopper2']."','".$row3['Hopper3']."','".$row3['Hopper4']."','".$row3['Validator1']."',
            '".$row3['Validator2']."','".$row3['CoinAcceptor']."', '".$row3['CreditCardSystem']."','".$row3['BillDispenser']."','".$row3['BillCassette1']."',
            '".$row3['BillCassette2']."','".$row3['BillCassette3']."','".$row3['BillCassette4']."','".$row3['BillDispenserReject']."','".$row3['BillDispenserMotor']."',
            '".$row3['BillDispenserQualifier']."','".$row3['BillDispenserDiverter']."','".$row3['BillDispenserStacker']."','".$row3['BillDispenserPresenter']."',
            '".$row3['BillDispenserData']."','".$row3['Display']."','".$row3['Programmer']."','".$row3['Printer1']."','".$row3['Printer2']."','".$row3['Temperature']."',
            '".$row3['Voltage']."','".$row3['Door']."','".$row3['LatestUpdate']."');\" 
            data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"This shows the data of kiosk ".$row['KioskName']."!\" 
            class=\"list-group-item\">".$row['KioskName']."</a>";
          }
          else{
            
            $list1 .= "<a onclick=\"showDetailsPanel('".$row['KioskName']."','".$row['KioskIpAddress']."','".$row['SerialNumber']."',
            '".$row['kioskNotes']."','".$row['idKiosk']."', '".$row2['idOperator']."','".$row2['FirstName']."','".$row2['LastName']."',
            '".$row2['Email']."','".$row2['loginUsername']."','".$row2['PhoneNumber']."',
            '0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0');\" 
            data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"This shows the data of kiosk ".$row['KioskName']."!\" 
            class=\"list-group-item\">".$row['KioskName']."</a>";
          }
        }
      }
      
      $list1 .= "</div></div>";
      
      echo "<h3>Machine List</h3>";
      echo $list1;
      
      $operator = $_SESSION['operator'];
      echo "<div class=\"col-xs-12\">";
      if ($operator == "American Changer" || $operator == "Test Operator"){
        echo "<button class=\"col-xs-offset-1 col-xs-3 btn btn-primary\" data-toggle=\"modal\" data-target=\"#modalAddKiosk\">Add New Kiosk</button>";
        echo "<button class=\"col-xs-offset-1 col-xs-3 btn btn-primary\" data-toggle=\"modal\" data-target=\"#modalEditOperator\">Edit Operator</button>";
        echo "<button class=\"col-xs-offset-1 col-xs-3 btn btn-danger\" data-toggle=\"modal\" data-target=\"#modalDeleteOperator\">Delete Operator</button>";
      }
      echo "</div>";
      
      echo "<p id=\"opId\" class=\"col-xs-1\" style=\"visibility:hidden\">".$row2['idOperator']."</p>
            <p id=\"opFN\" class=\"col-xs-1\" style=\"visibility:hidden\">".$row2['FirstName']."</p>
            <p id=\"opLN\" class=\"col-xs-1\" style=\"visibility:hidden\">".$row2['LastName']."</p>
            <p id=\"opEmail\" class=\"col-xs-1\" style=\"visibility:hidden\">".$row2['Email']."</p>
            <p id=\"opUN\" class=\"col-xs-1\" style=\"visibility:hidden\">".$row2['loginUsername']."</p> 
            <p id=\"opPhone\" class=\"col-xs-1\" style=\"visibility:hidden\">".$row2['PhoneNumber']."</p> 
          <p id=\"opType\" class=\"col-xs-1\" style=\"visibility:hidden\">".$row2['OpReportType_idReportType']."</p> 
        ";
      mysqli_close($db_link2);
      
    }
    
    if ($command == "getOperatorList"){
      $Operator = $_GET['Operator'];
      $Page = $_GET['page'];
      if ($Operator == "American Changer" or $Operator == "Test Operator"){
        $sql = "select OperatorName from operator";
      }
      else{
        $sql = "select OperatorName from operator where OperatorName = '".$Operator."'";
      }
      
      //$sql = "select * from Kiosk WHERE idKiosk = '".$q."'";	
      $result = mysqli_query($db_link,$sql);
      
      $select1="";
      
      
      $select1 .=  "<select class=\"form-control\" id=\"operator\" onchange=\"loadUser(this.value, '".$Page."')\">";
      
      if ($Operator == "American Changer" || $Operator == "Test Operator" ){
        $select1 .= "<option value=\"\">Select an Operator</option>";
      }
      else{
        $select1 .= "<option value=\"\">Select an Operator</option>";
      }
      
        
      while($row = mysqli_fetch_array($result)) {
        if ($Operator == "American Changer"){
          if ($row['OperatorName'] != "Test Operator" && $row['OperatorName'] != "Test Op"){
            $select1 .= "<option value='" . $row['OperatorName'] ." '>" . $row['OperatorName'] . "</option>";
          }
        }
        else{
          $select1 .= "<option value='" . $row['OperatorName'] ." '>" . $row['OperatorName'] . "</option>";
        }
        
      }
      
    
      $select1 .=  "</select>";	
      $operator = $_SESSION['operator'];
      
      echo $select1;		
    }

    if ($command == "getReadersList"){
      $Operator = $_SESSION['operator'];

      $sql = "call getUndefinedReaders('".$Operator."')";
      $result = mysqli_query($db_link,$sql);

      $select = '<select multiple class="form-control readerSelect" id="ReaderSelect">';
      while($row = mysqli_fetch_array($result)) {
        $select .= "<option class='blue' value='" . $row['idReaderConfig'] ." '>" . $row['ReaderDeviceId'] . "</option>";
      }

      $select .= "</select>";

      echo $select;
    }
    
    if ($command == "getOperatorList2"){
      
      $Operator = $_GET['Operator'];
      
      if ($Operator == "American Changer" or $Operator == "Test Operator"){
        $sql = "select OperatorName from operator";
      }
      
      else{
        $sql = "select OperatorName from operator where OperatorName = '".$Operator."'";
      }
      
      //$sql = "select * from Kiosk WHERE idKiosk = '".$q."'";	
      $result = mysqli_query($db_link,$sql);
      
      $select1="";

      $select1 .=  "<select Id=\"operator\" class=\"form-control\" onchange=\"loadUser2(this.value)\">
        <option value=\"\">Select an Operator</option>";
        
      
      
        
      while($row = mysqli_fetch_array($result)) {
        if ($Operator == "American Changer"){
          if ($row['OperatorName'] != "Test Operator"){
            $select1 .= "<option value='" . $row['OperatorName'] ." '>" . $row['OperatorName'] . "</option>";
          }
        }
        else{
          $select1 .= "<option value='" . $row['OperatorName'] ." '>" . $row['OperatorName'] . "</option>";
        }
        
      }
      
    
      $select1 .=  "</select>";	
      
      echo $select1;
          
    }
    
    if ($command == "getOperatorListIdx"){
      
      $sql = "call getOperatorList()";
      $result = mysqli_query($db_link,$sql);
      $select1="";
      $select1 .=  "<select class=\"form-control\" align-center name=\"operator\">";	
      while($row = mysqli_fetch_array($result)) {
        $select1 .= "<option value='" . $row['OperatorName'] ." '>" . $row['OperatorName'] . "</option>";
      }
      $select1 .=  "</select>";	
      
      
      echo "<div class=\"mbr-white col-md-8 offset-md-2\">";
      echo $select1;
      echo "</div>";
      
    }
    
    if ($command == "getUserBalance"){
      
      $operator = $_SESSION['operator'];
      $userId = $_SESSION['userId'];
      $sql = "call getUserBalance('".$userId."');";
      $result = mysqli_query($db_link,$sql);
      if ($row = mysqli_fetch_array($result)) 
      {
        $UserBalance = $row['VirtualCardBalance'];
        echo ' <div class="row justify-content-center">
              <div class="plan lg col-12 mx-2 my-2 justify-content-center">
                <div class="plan-header text-center pt-5">
                  <br><br><br><br>
                  <h3 class="plan-title mbr-fonts-style display-5">
                    You currently have</h3>
                  <br>
                  <div class="plan-price">
                    <span class="price-figure mbr-fonts-style display-1">
                        '.$UserBalance.' credits</span>
                  </div>
                </div>
                <div class="plan-body pb-5">
                  <br>
                  <div class="plan-list align-center">
                    <ul class="mbr-fonts-style display-5">
                      available at '.$operator.'
                    </ul>
                  </div>
                  <br><br>
                  <div class="mbr-section-btn text-center pt-4">
                    <a href="home.php" class="btn btn-primary display-4">Add more credits</a>
                  </div>
                  <br><br>
                </div>
              </div>
            </div>';
      }
      else
      {
        echo ' <div class="row justify-content-center">
              <div class="plan lg col-12 mx-2 my-2 justify-content-center">
                <div class="plan-header text-center pt-5">
                  <br><br><br><br>
                  <h3 class="plan-title mbr-fonts-style display-5">
                    We are so sorry</h3>
                  <br>
                </div>
                <div class="plan-body pb-5">
                  <br>
                  <div class="plan-list align-center">
                    <ul class="list-group list-group-flush mbr-fonts-style display-5">
                      We could not retrieve your account balance
                    </ul>
                  </div>
                  <br><br>
                  <div class="mbr-section-btn text-center pt-4">
                    <a href="balance.php" class="btn btn-primary display-4">Please Try Again</a>
                  </div>
                  <br><br>
                </div>
              </div>
            </div>';
      }
    }
    
    if ($command == "getSpecialOffersOp"){
      
      $operator = $_SESSION['operator'];
      $sql = "call getSpecialOffersData('".$operator."');";
      $result = mysqli_query($db_link,$sql);
      $offers="";
      while($row = mysqli_fetch_array($result)) {
        echo '
          <div class="plan col-12 mx-2 my-2 justify-content-center col-lg-3">
            <div class="plan-header text-center pt-5">
              <h3 class="plan-title mbr-fonts-style display-5">
                '.$row['SpecialOfferName'].'</h3>
              <div class="plan-price">
                <span class="price-figure mbr-fonts-style display-1">
                    '.$row['SpecialOfferCredits'].'</span>
                <small class="price-term mbr-fonts-style display-7">credits</small>
              </div>
            </div>
            <div class="plan-body pb-5">
              <div class="plan-list align-center">
                for $'.$row['SpecialOfferValue'].'
              </div>
              <div class="mbr-section-btn text-center pt-4">
              <a href="" class="btn btn-primary display-4" data-toggle="modal" data-target="#modalDeleteOffer" 
              data-whatever="'.$row['SpecialOfferName'].'" data-id="'.$row['idSpecialOffers'].'">Remove</a>
              </div>
            </div>
          </div>'; //<a class="btn btn-primary display-4">&nbsp; &nbsp; Edit &nbsp;&nbsp;</a> 
      }
    }

    if ($command == "getSpecialOffers"){
      
      $operator = $_SESSION['operator'];
      $sql = "call getSpecialOffersData('".$operator."');";
      $result = mysqli_query($db_link,$sql);
      $offers="";
      // Everytime home is reloaded restart OfferList
      $_SESSION['offerList'] = array();
      while($row = mysqli_fetch_array($result)) {
        // save data in Session variable to improve loading times
        $newOfferEntry = array($row['idSpecialOffers'],
                              $row['SpecialOfferName'],
                              $row['SpecialOfferValue'],
                              $row['SpecialOfferCredits']);
        array_push($_SESSION['offerList'], $newOfferEntry);

        echo '<div class="plan col-12 mx-2 my-2 justify-content-center col-lg-4">
                <div class="plan-header text-center pt-5">
                  <h3 class="plan-title mbr-fonts-style display-5">
                    '.$row['SpecialOfferName'].'</h3>
                  <div class="plan-price">
                    <span class="price-figure mbr-fonts-style display-1">
                        '.$row['SpecialOfferCredits'].'</span>
                    <small class="price-term mbr-fonts-style display-7">credits</small>
                  </div>
                </div>
                <div class="plan-body pb-5">
                  <div class="plan-list align-center">
                    for $'.$row['SpecialOfferValue'].'
                  </div>
                  <div class="mbr-section-btn text-center pt-4">
                    <button class="btn btn-primary display-4"
                      onclick="addToCart('.$row['idSpecialOffers'].',
                                        \''.$row['SpecialOfferName'].'\',
                                        '.$row['SpecialOfferValue'].',
                                        '.$row['SpecialOfferCredits'].')">
                      ADD TO CART
                    </button>
                  </div>
                </div>
              </div>';
        
      }
      
    }
    
    if ($command == "addToCart"){
      
      $operator = $_SESSION['operator'];
      $idOffer = $_GET['itemId'];
      $offerName = $_GET['Name'];
      $offerValue = $_GET['Value'];
      $offerCredits = $_GET['Credits'];
      $itemsInCart = $_SESSION['itemsInCart'];
      if (!$itemsInCart){
        $itemsInCart = 0;
        $_SESSION['itemList'] = array();
      }

      $itemExists = False;
      // $item[0] = id
      foreach($_SESSION['itemList'] as $key=>$item){
        if ($item[0] == $idOffer){
          $_SESSION['itemList'][$key][4] += 1;
          // mark the item as existent to avoid inserting again
          $itemExists = True;
        }
      }
      // if item does not exist insert a new entry
      if ($itemExists == False){
        $newItemEntry = array($idOffer, $offerName, $offerValue, $offerCredits, 1);
        array_push($_SESSION['itemList'], $newItemEntry);
      }
      

      $itemsInCart+=1; 
      $_SESSION['itemsInCart'] = $itemsInCart;
      echo '<div id="cartIcon">
              <span class="cart">
                <text>'.$_SESSION['itemsInCart'].'</text>
              </span>
            </div>';
    }

    if ($command == "addToCartSidebar"){
      
      $operator = $_SESSION['operator'];
      $idOffer = $_GET['itemId'];
      $offerName = $_GET['Name'];
      $offerValue = $_GET['Value'];
      $offerCredits = $_GET['Credits'];
      $itemsInCart = $_SESSION['itemsInCart'];
      if (!$itemsInCart){
        $itemsInCart = 0;
        $_SESSION['itemList'] = array();
      }

      $itemExists = False;
      // $item[0] = id
      foreach($_SESSION['itemList'] as $key=>$item){
        if ($item[0] == $idOffer){
          $_SESSION['itemList'][$key][4] += 1;
          // mark the item as existent to avoid inserting again
          $itemExists = True;
        }
      }
      // if item does not exist insert a new entry
      if ($itemExists == False){
        $newItemEntry = array($idOffer, $offerName, $offerValue, $offerCredits, 1);
        array_push($_SESSION['itemList'], $newItemEntry);
      }
      
      $itemsInCart+=1; 
      $_SESSION['itemsInCart'] = $itemsInCart;
      /*  item[0] = id
        item[1] = name
        item[2] = value
        item[3] = credits
        item[4] = quantity */
        $total = 0;
        echo '<table class="table">
              <thead>
                <tr class="d-flex">
                  <th class="col-4" style="text-align: left;">Item</th>
                  <th class="col-4">Quantity</th>
                  <th class="col-2">Price</th>
                  <th class="col-1"></th>
                </tr>
              </thead>
              <tbody>';
        foreach($_SESSION['itemList'] as $key=>$item)
        {
          $total += $item[4]*$item[2];
          echo '<tr class="d-flex">
                  <td class="col-4">
                    <div class="tableItem">
                      <div class="name">
                        '.$item[1].' 
                      </div>
                      <div class="description">
                        '.$item[3].' credits
                      </div>
                    </div>
                  </td>
                  <td class="col-4">
                    <a class="item-control" onclick=removeItem('.$key.')>-</a>
                      '.$item[4].'
                    <a class="item-control" onclick=addItem('.$key.')>+</a></td>
                  <td class="col-2">$'.$item[2].'</td>
                  <td class="col-1">
                    <span class="deleteItem" onclick=deleteItem('.$key.')></span>
                  </td>
                </tr>';
        }
        $_SESSION['totalPurchase']=$total;
        echo '<tr class="d-flex">
            <td class="col-8" style="text-align: left;">Total</td>
            <td class="col-2">$'.$_SESSION['totalPurchase'].'</td>
            </tr>
          </tbody>
        </table>';
        echo '
        <div class="d-flex justify-content-center main-add">';
        // Check for cart item quantity
        if ($_SESSION['itemsInCart'] <= 0){
          echo '<a href="" class="btn btn-secondary" data-toggle="modal" data-target="#modalItems">Proceed to checkout</a>';
        }
        else{
          // Check if the user is signed in
          if (isset($_SESSION['username'])){
            echo '<a class="btn btn-secondary" href="checkout.php">Proceed to checkout</a>';
          }
          else{
            echo '<a href="" class="btn btn-secondary" data-toggle="modal" data-target="#modalLogin">Proceed to checkout</a>';
          }
        }
    }

    if ($command == "addItem"){
      
      $arrayKey = $_GET['arrayKey'];
      $_SESSION['itemList'][$arrayKey][4] += 1;
      $_SESSION['itemsInCart'] += 1;

      /*  item[0] = id
        item[1] = name
        item[2] = value
        item[3] = credits
        item[4] = quantity */
        $total = 0;
        echo '<table class="table">
              <thead>
                <tr class="d-flex">
                  <th class="col-4" style="text-align: left;">Item</th>
                  <th class="col-4">Quantity</th>
                  <th class="col-2">Price</th>
                  <th class="col-1"></th>
                </tr>
              </thead>
              <tbody>';
        foreach($_SESSION['itemList'] as $key=>$item)
        {
          $total += $item[4]*$item[2];
          echo '<tr class="d-flex">
                  <td class="col-4">
                    <div class="tableItem">
                      <div class="name">
                        '.$item[1].' 
                      </div>
                      <div class="description">
                        '.$item[3].' credits
                      </div>
                    </div>
                  </td>
                  <td class="col-4">
                    <a class="item-control" onclick=removeItem('.$key.')>-</a>
                      '.$item[4].'
                    <a class="item-control" onclick=addItem('.$key.')>+</a></td>
                  <td class="col-2">$'.$item[2].'</td>
                  <td class="col-1">
                    <span class="deleteItem" onclick=deleteItem('.$key.')></span>
                  </td>
                </tr>';
        }
        $_SESSION['totalPurchase']=$total;
        echo '<tr class="d-flex">
            <td class="col-8" style="text-align: left;">Total</td>
            <td class="col-2">$'.$_SESSION['totalPurchase'].'</td>
            </tr>
          </tbody>
        </table>';
        echo '
        <div class="d-flex justify-content-center main-add">';
        // Check for cart item quantity
        if ($_SESSION['itemsInCart'] <= 0){
          echo '<a href="" class="btn btn-secondary" data-toggle="modal" data-target="#modalItems">Proceed to checkout</a>';
        }
        else{
          // Check if the user is signed in
          if (isset($_SESSION['username'])){
            echo '<a class="btn btn-secondary" href="checkout.php">Proceed to checkout</a>';
          }
          else{
            echo '<a href="" class="btn btn-secondary" data-toggle="modal" data-target="#modalLogin">Proceed to checkout</a>';
          }
        }
    }

    if ($command == "removeItem"){
      
      $arrayKey = $_GET['arrayKey'];
      $_SESSION['itemList'][$arrayKey][4] -= 1;
      $_SESSION['itemsInCart'] -= 1;
      if ($_SESSION['itemList'][$arrayKey][4] == 0)
      {
        unset($_SESSION['itemList'][$arrayKey]);
        array_values($_SESSION['itemList']);
      }

      /*  item[0] = id
        item[1] = name
        item[2] = value
        item[3] = credits
        item[4] = quantity */
        $total = 0;
        echo '<table class="table">
              <thead>
                <tr class="d-flex">
                  <th class="col-4" style="text-align: left;">Item</th>
                  <th class="col-4">Quantity</th>
                  <th class="col-2">Price</th>
                  <th class="col-1"></th>
                </tr>
              </thead>
              <tbody>';
        foreach($_SESSION['itemList'] as $key=>$item)
        {
          $total += $item[4]*$item[2];
          echo '<tr class="d-flex">
                  <td class="col-4">
                    <div class="tableItem">
                      <div class="name">
                        '.$item[1].' 
                      </div>
                      <div class="description">
                        '.$item[3].' credits
                      </div>
                    </div>
                  </td>
                  <td class="col-4">
                    <a class="item-control" onclick=removeItem('.$key.')>-</a>
                      '.$item[4].'
                    <a class="item-control" onclick=addItem('.$key.')>+</a></td>
                  <td class="col-2">$'.$item[2].'</td>
                  <td class="col-1">
                    <span class="deleteItem" onclick=deleteItem('.$key.')></span>
                  </td>
                </tr>';
        }
        $_SESSION['totalPurchase']=$total;
        echo '<tr class="d-flex">
            <td class="col-8" style="text-align: left;">Total</td>
            <td class="col-2">$'.$_SESSION['totalPurchase'].'</td>
            </tr>
          </tbody>
        </table>';
        echo '
        <div class="d-flex justify-content-center main-add">';
        // Check for cart item quantity
        if ($_SESSION['itemsInCart'] <= 0){
          echo '<a href="" class="btn btn-secondary" data-toggle="modal" data-target="#modalItems">Proceed to checkout</a>';
        }
        else{
          // Check if the user is signed in
          if (isset($_SESSION['username'])){
            echo '<a class="btn btn-secondary" href="checkout.php">Proceed to checkout</a>';
          }
          else{
            echo '<a href="" class="btn btn-secondary" data-toggle="modal" data-target="#modalLogin">Proceed to checkout</a>';
          }
        }
    }

    if ($command == "deleteItem"){
      
      $arrayKey = $_GET['arrayKey'];

      //Remove the quantity from the global item count
      $_SESSION['itemsInCart'] -= $_SESSION['itemList'][$arrayKey][4];

      //Unset the itemList and rearrange the array
      unset($_SESSION['itemList'][$arrayKey]);
      array_values($_SESSION['itemList']);

      /*  item[0] = id
        item[1] = name
        item[2] = value
        item[3] = credits
        item[4] = quantity */
        $total = 0;
        echo '<table class="table">
              <thead>
                <tr class="d-flex">
                  <th class="col-4" style="text-align: left;">Item</th>
                  <th class="col-4">Quantity</th>
                  <th class="col-2">Price</th>
                  <th class="col-1"></th>
                </tr>
              </thead>
              <tbody>';
        foreach($_SESSION['itemList'] as $key=>$item)
        {
          $total += $item[4]*$item[2];
          echo '<tr class="d-flex">
                  <td class="col-4">
                    <div class="tableItem">
                      <div class="name">
                        '.$item[1].' 
                      </div>
                      <div class="description">
                        '.$item[3].' credits
                      </div>
                    </div>
                  </td>
                  <td class="col-4">
                    <a class="item-control" onclick=removeItem('.$key.')>-</a>
                      '.$item[4].'
                    <a class="item-control" onclick=addItem('.$key.')>+</a></td>
                  <td class="col-2">$'.$item[2].'</td>
                  <td class="col-1">
                    <span class="deleteItem" onclick=deleteItem('.$key.')></span>
                  </td>
                </tr>';
        }
        $_SESSION['totalPurchase']=$total;
        echo '<tr class="d-flex">
            <td class="col-8" style="text-align: left;">Total</td>
            <td class="col-2">$'.$_SESSION['totalPurchase'].'</td>
            </tr>
          </tbody>
        </table>';
        echo '
        <div class="d-flex justify-content-center main-add">';
        // Check for cart item quantity
        if ($_SESSION['itemsInCart'] <= 0){
          echo '<a href="" class="btn btn-secondary" data-toggle="modal" data-target="#modalItems">Proceed to checkout</a>';
        }
        else{
          // Check if the user is signed in
          if (isset($_SESSION['username'])){
            echo '<a class="btn btn-secondary" href="checkout.php">Proceed to checkout</a>';
          }
          else{
            echo '<a href="" class="btn btn-secondary" data-toggle="modal" data-target="#modalLogin">Proceed to checkout</a>';
          }
        }
    }

    if ($command == "payNowButton"){
      
      echo '
          <div class="main-header" style="padding-left: 20rem;">
            Your payment has been completed!
          </div>
          <div class="d-flex justify-content-start main-body" style="text-align:left; padding-left: 20rem;">
            <div class="checkout" style="padding-left: 0rem;">
              <div class="title">Your Order</div>';

      foreach($_SESSION['itemList'] as $key=>$item)
      {
        echo '<div class="item-name">
                '.$item[1].'
              </div>
              <div class="item-qty">
                Quantity: '.$item[4].'
              </div>';
      }
      echo '<div class="total">
              Total: $'.$_SESSION['totalPurchase'].'
            </div>
          </div>
          </div>
          <div class="d-flex justify-content-center main-add">
            <a class="btn btn-secondary wide" href="balance.php">Check Balance</a>
          </div>';
    }

    if ($command == "getReaderTransactions"){
      $dateLow = $_GET['dateLow'];
      $dateHigh = $_GET['dateHigh'];
      $readerID = $_GET['idReader'];
      
      if ($readerID == 'all')
      {
        $readerID = 0;
      }

      $sql = "call getReaderTransactionsOp(".$readerID.",'".$_SESSION['operatorID']."',".$dateHigh.",".$dateLow.")";
      $result = mysqli_query($db_link,$sql);
      
      $table = '
        <table class="table">
          <thead>
          <tr class="table-heads ">
            <th class="head-item mbr-fonts-style display-7">Date</th> 
            <th class="head-item mbr-fonts-style display-7">Reader ID</th>
            <th class="head-item mbr-fonts-style display-7">Reader Mode</th>
            <th class="head-item mbr-fonts-style display-7">Card Serial Num.</th>
            <th class="head-item mbr-fonts-style display-7">Prev Balance</th>
            <th class="head-item mbr-fonts-style display-7">New Balance</th>
          </tr>
          </thead>
          <tbody>';
        $textData = "Date, Reader ID, Reader Mode, Card Serial Num., Prev Balance, New Balance";
        while($row = mysqli_fetch_array($result)) {
            $table .=
            "<tr>
            <td class='body-item mbr-fonts-style display-7'>".$row['TransactionTime']."</td>
            <td class='body-item mbr-fonts-style display-7'>".$row['ReaderID']."</td>
            <td class='body-item mbr-fonts-style display-7'>".$row['RFModeDescription']."</td>
            <td class='body-item mbr-fonts-style display-7'>".$row['VirtualCardSerial']."</td>
            <td class='body-item mbr-fonts-style display-7'>".$row['OldCardBalance']."</td>
            <td class='body-item mbr-fonts-style display-7'>".$row['NewCardBalance']."</td>
            </tr>";
            $textData .=  "|".$row['TransactionTime'].
                        ",".$row['ReaderID'].
                        ",".$row['RFModeDescription'].
                        ",".$row['VirtualCardSerial'].
                        ",".$row['OldCardBalance'].
                        ",".$row['NewCardBalance'];
        }
      $table .= '</tbody>
        </table>';
      
      $reportName = "Reader_Transactions";
      echo "<div class='form-group col-sm-6'>
              <button class='btn btn-secondary' type='submit'
                  onclick='download_csv(\"".$reportName."\",\"".$textData."\")'>Download CSV</button>
            </div>";
      echo $table;
    }

    if ($command == "getKioskTransactions"){
      $dateLow = $_GET['dateLow'];
      $dateHigh = $_GET['dateHigh'];
      $kioskID = $_GET['idKiosk'];
      
      if ($kioskID == 'all')
      {
        $kioskID = 0;
      }

      $sql = "call getKioskTransactions(".$kioskID.",'".$_SESSION['operatorID']."',".$dateHigh.",".$dateLow.")";
      $result = mysqli_query($db_link,$sql);

      $table = '
      <table class="table">
          <thead>
          <tr class="table-heads ">
            <th class="head-item mbr-fonts-style display-7">Date</th> 
            <th class="head-item mbr-fonts-style display-7">Kiosk ID</th>
            <th class="head-item mbr-fonts-style display-7">Amount $</th>
            <th class="head-item mbr-fonts-style display-7">Card ID</th>
            <th class="head-item mbr-fonts-style display-7">Prev Balance</th>
            <th class="head-item mbr-fonts-style display-7">New Balance</th>
            <th class="head-item mbr-fonts-style display-7">Package Name</th>
            <th class="head-item mbr-fonts-style display-7">Package Qty</th>
          </tr>
          </thead>
          <tbody>';
      $textData = "Date, Kiosk ID, Amount $, Card ID, Prev Balance, New Balance, Package Name, Package Qty";
      while($row = mysqli_fetch_array($result)) {
          $table .=
          "<tr>
          <td class='body-item mbr-fonts-style display-7'>".$row['pDatetime']."</td>
          <td class='body-item mbr-fonts-style display-7'>".$row['KioskConfigId']."</td>
          <td class='body-item mbr-fonts-style display-7'>".$row['PurchaseAmount']."</td>
          <td class='body-item mbr-fonts-style display-7'>".$row['CardSerialNumber']."</td>
          <td class='body-item mbr-fonts-style display-7'>".$row['PreviousCardBalance']."</td>
          <td class='body-item mbr-fonts-style display-7'>".$row['NewCardBalance']."</td>
          <td class='body-item mbr-fonts-style display-7'>".$row['PackageName']."</td>
          <td class='body-item mbr-fonts-style display-7'>".$row['PackagesQuantity']."</td>
          </tr>";
          $textData .=  "|".$row['pDatetime'].
                        ",".$row['KioskConfigId'].
                        ",".$row['PurchaseAmount'].
                        ",".$row['CardSerialNumber'].
                        ",".$row['PreviousCardBalance'].
                        ",".$row['NewCardBalance'].
                        ",".$row['PackageName'].
                        ",".$row['PackagesQuantity'];
      }
      $table .= '</tbody>
        </table>';

      $reportName = "Kiosk_Transactions";
      echo "<div class='form-group col-sm-6'>
              <button class='btn btn-secondary' type='submit'
                  onclick='download_csv(\"".$reportName."\",\"".$textData."\")'>Download CSV</button>
            </div>";
      echo $table;
    }
    
    if ($command == "getTicketsPerCard"){
      
      $dateLow = $_GET['dateLow'];
      $dateHigh = $_GET['dateHigh'];

      $cardID = $_GET['cardID'];
      
      if ($kioskID == 'all')
      {
        $kioskID = 0;
      }

      $sql = "call getTicketsPerCard('".$_SESSION['operatorID']."',".$cardID.",".$dateHigh.",".$dateLow.")";
      $result = mysqli_query($db_link,$sql);

      $table = '
      <table class="table">
          <thead>
          <tr class="table-heads ">
            <th class="head-item mbr-fonts-style display-7">Date</th>
            <th class="head-item mbr-fonts-style display-7">Reader ID</th>
            <th class="head-item mbr-fonts-style display-7">Ticket Quantity</th>
            <th class="head-item mbr-fonts-style display-7">Reader Mode</th>
          </tr>
          </thead>
          <tbody>';
      $textData = "Date, Reader ID, Ticket Quantity, Reader Mode";
        while($row = mysqli_fetch_array($result)) {
            $table .=
            "<tr>
            <td class='body-item mbr-fonts-style display-7'>".$row['TransactionTime']."</td>
            <td class='body-item mbr-fonts-style display-7'>".$row['ReaderDeviceId']."</td>
            <td class='body-item mbr-fonts-style display-7'>".$row['TicketQty']."</td>
            <td class='body-item mbr-fonts-style display-7'>".$row['RFModeDescription']."</td>
            </tr>";
            $textData .= "|".$row['TransactionTime'].","
                            .$row['ReaderDeviceId'].","
                            .$row['TicketQty'].","
                            .$row['RFModeDescription'];
        }
      $table .= '</tbody>
      </table>';

      $reportName = "RF_Card_Tickets";
      echo "<div class='form-group col-sm-6'>
      <button class='btn btn-secondary' type='submit'
      onclick='download_csv(\"".$reportName."\",\"".$textData."\")'>Download CSV</button>
      </div>";
      echo $table;
    }

    if ($command == "getCreditsPerCard"){
      
      $dateLow = $_GET['dateLow'];
      $dateHigh = $_GET['dateHigh'];

      $cardID = $_GET['cardID'];
      
      if ($kioskID == 'all')
      {
        $kioskID = 0;
      }

      $sql = "call getCreditsPerCard('".$_SESSION['operatorID']."',".$cardID.",".$dateHigh.",".$dateLow.")";
      $result = mysqli_query($db_link,$sql);

      $table = '
      <table class="table">
          <thead>
          <tr class="table-heads ">
            <th class="head-item mbr-fonts-style display-7">Date</th>
            <th class="head-item mbr-fonts-style display-7">Reader ID</th>
            <th class="head-item mbr-fonts-style display-7">Prev Balance</th>
            <th class="head-item mbr-fonts-style display-7">New Balance</th>
            <th class="head-item mbr-fonts-style display-7">Reader Mode</th>
          </tr>
          </thead>
          <tbody>';
      $textData = "Date, Reader ID, Prev Balance, New Balance, Reader Mode";
        while($row = mysqli_fetch_array($result)) {
            $table .=
            "<tr>
            <td class='body-item mbr-fonts-style display-7'>".$row['TransactionTime']."</td>
            <td class='body-item mbr-fonts-style display-7'>".$row['ReaderDeviceId']."</td>
            <td class='body-item mbr-fonts-style display-7'>".$row['OldCardBalance']."</td>
            <td class='body-item mbr-fonts-style display-7'>".$row['NewCardBalance']."</td>
            <td class='body-item mbr-fonts-style display-7'>".$row['RFModeDescription']."</td>
            </tr>";
            $textData .= "|".$row['TransactionTime'].","
                            .$row['ReaderDeviceId'].","
                            .$row['OldCardBalance'].","
                            .$row['NewCardBalance'].","
                            .$row['RFModeDescription'];
        }
      $table .= '</tbody>
      </table>';

      $reportName = "Ticket_Redemptions";
      echo "<div class='form-group col-sm-6'>
      <button class='btn btn-secondary' type='submit'
      onclick='download_csv(\"".$reportName."\",\"".$textData."\")'>Download CSV</button>
      </div>";
      echo $table;
    }

    if ($command == "getReaderCash"){
      
      $dateLow = $_GET['dateLow'];
      $dateHigh = $_GET['dateHigh'];
      
      if ($kioskID == 'all')
      {
        $kioskID = 0;
      }

      $sql = "call getReaderCash('".$_SESSION['operatorID']."',".$dateHigh.",".$dateLow.")";
      $result = mysqli_query($db_link,$sql);

      $table = '
      <table class="table">
          <thead>
          <tr class="table-heads ">
            <th class="head-item mbr-fonts-style display-7">Date</th> 
            <th class="head-item mbr-fonts-style display-7">Reader ID</th>
            <th class="head-item mbr-fonts-style display-7">Cash Received</th>
          </tr>
          </thead>
          <tbody>';
      $textData = "Date, Reader ID, Cash Received";
        while($row = mysqli_fetch_array($result)) {
          $table .= 
            "<tr>
            <td class='body-item mbr-fonts-style display-7'>".$row['TransactionTime']."</td>
            <td class='body-item mbr-fonts-style display-7'>".$row['ReaderId']."</td>
            <td class='body-item mbr-fonts-style display-7'>".$row['CashQty']."</td>
            </tr>";
            $textData .= "|".$row['TransactionTime'].","
                            .$row['ReaderId'].","
                            .$row['CashQty'];
        }
      $table .= '</tbody>
      </table>';

      $reportName = "Reader_Cash_Tracking";
      echo "<div class='form-group col-sm-6'>
      <button class='btn btn-secondary' type='submit'
      onclick='download_csv(\"".$reportName."\",\"".$textData."\")'>Download CSV</button>
      </div>";
      echo $table;
    }

    if ($command == "getRedemptionReport"){
      
      $dateLow = $_GET['dateLow'];
      $dateHigh = $_GET['dateHigh'];
      
      if ($kioskID == 'all')
      {
        $kioskID = 0;
      }

      $sql = "call getRedemptionReport('".$_SESSION['operatorID']."',".$dateHigh.",".$dateLow.")";
      $result = mysqli_query($db_link,$sql);

      $table = '
      <table class="table">
          <thead>
          <tr class="table-heads ">
            <th class="head-item mbr-fonts-style display-7">Date</th> 
            <th class="head-item mbr-fonts-style display-7">Card ID</th>
            <th class="head-item mbr-fonts-style display-7">Reader ID</th>
            <th class="head-item mbr-fonts-style display-7">Prev Balance</th>
            <th class="head-item mbr-fonts-style display-7">New Balance</th>
          </tr>
          </thead>
          <tbody>';
        $textData = "Date, Card ID, Reader ID, Prev Balance, New Balance";
        while($row = mysqli_fetch_array($result)) {
            $table .=
            "<tr>
            <td class='body-item mbr-fonts-style display-7'>".$row['TransactionTime']."</td>
            <td class='body-item mbr-fonts-style display-7'>".$row['VirtualCardSerial']."</td>
            <td class='body-item mbr-fonts-style display-7'>".$row['ReaderDeviceId']."</td>
            <td class='body-item mbr-fonts-style display-7'>".$row['OldCardBalance']."</td>
            <td class='body-item mbr-fonts-style display-7'>".$row['NewCardBalance']."</td>
            </tr>";
            $textData .= "|".$row['TransactionTime'].","
                            .$row['VirtualCardSerial'].","
                            .$row['ReaderDeviceId'].","
                            .$row['OldCardBalance'].","
                            .$row['NewCardBalance'];
        }
      $table .= '</tbody>
        </table>';

      $reportName = "Ticket_Redemptions";
      echo "<div class='form-group col-sm-6'>
      <button class='btn btn-secondary' type='submit'
      onclick='download_csv(\"".$reportName."\",\"".$textData."\")'>Download CSV</button>
      </div>";
      echo $table;
    }

    if ($command == "getDumpReport"){
      
      $dateLow = $_GET['dateLow'];
      $dateHigh = $_GET['dateHigh'];
      
      if ($kioskID == 'all')
      {
        $kioskID = 0;
      }

      $sql = "call getDumpReport('".$_SESSION['operatorID']."',".$dateHigh.",".$dateLow.")";
      $result = mysqli_query($db_link,$sql);

      $table .= '
      <table class="table">
          <thead>
          <tr class="table-heads ">
            <th class="head-item mbr-fonts-style display-7">Date</th> 
            <th class="head-item mbr-fonts-style display-7">Card ID</th>
            <th class="head-item mbr-fonts-style display-7">Reader ID</th>
            <th class="head-item mbr-fonts-style display-7">Prev Balance</th>
            <th class="head-item mbr-fonts-style display-7">New Balance</th>
          </tr>
          </thead>
          <tbody>';
      $textData = "Date, Card ID, Reader ID, Prev Balance, New Balance";
        while($row = mysqli_fetch_array($result)) {
            $table .=
            "<tr>
            <td class='body-item mbr-fonts-style display-7'>".$row['TransactionTime']."</td>
            <td class='body-item mbr-fonts-style display-7'>".$row['VirtualCardSerial']."</td>
            <td class='body-item mbr-fonts-style display-7'>".$row['ReaderDeviceId']."</td>
            <td class='body-item mbr-fonts-style display-7'>".$row['OldCardBalance']."</td>
            <td class='body-item mbr-fonts-style display-7'>".$row['NewCardBalance']."</td>
            </tr>";
            $textData .= "|".$row['TransactionTime'].","
                            .$row['VirtualCardSerial'].","
                            .$row['ReaderDeviceId'].","
                            .$row['OldCardBalance'].","
                            .$row['NewCardBalance'];
        }
      $table .= '</tbody>
        </table>';

      $reportName = "Card_Dump_Operations";
      echo "<div class='form-group col-sm-6'>
              <button class='btn btn-secondary' type='submit'
                  onclick='download_csv(\"".$reportName."\",\"".$textData."\")'>Download CSV</button>
            </div>";
      echo $table;
    }

    if ($command == "getCashAccepted"){
      $dateLow = $_GET['dateLow'];
      $dateHigh = $_GET['dateHigh'];
      $reportType = $_GET['reportType'];
      
      $lastKioskBuffer = "";

      $sql = "call getKioskCashAccepted(".$_SESSION['operatorID'].",'".$reportType."',".$dateLow.",".$dateHigh.")";
      $result = mysqli_query($db_link,$sql);
      
      $table = '
      <table class="table">
          <thead>
          <tr class="table-heads ">
            <th class="head-item mbr-fonts-style display-7">Kiosk ID</th>';
      $textData = "Kiosk ID,";
      switch ($reportType)
      {
        default:
        case "hour":
          $variableLen = 24;
          for ($hour = 0;$hour < $variableLen; $hour++)
          {
            $table .= '<th class="head-item mbr-fonts-style display-7">'.$hour.'-'.($hour+1).'</th>';
            $textData .= $hour."-".($hour+1).",";
          }
          break;
        case "weekday":
          $variableLen = 7;
          $table .= '<th class="head-item mbr-fonts-style display-7">Monday</th>
                <th class="head-item mbr-fonts-style display-7">Tuesday</th>
                <th class="head-item mbr-fonts-style display-7">Wednesday</th>
                <th class="head-item mbr-fonts-style display-7">Thursday</th>
                <th class="head-item mbr-fonts-style display-7">Friday</th>
                <th class="head-item mbr-fonts-style display-7">Saturday</th>
                <th class="head-item mbr-fonts-style display-7">Sunday</th>';
          $textData .= "Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday,";
          break;
        case "month":
          $variableLen = 12;
          $table .= '<th class="head-item mbr-fonts-style display-7">January</th>
                <th class="head-item mbr-fonts-style display-7">February</th>
                <th class="head-item mbr-fonts-style display-7">March</th>
                <th class="head-item mbr-fonts-style display-7">April</th>
                <th class="head-item mbr-fonts-style display-7">May</th>
                <th class="head-item mbr-fonts-style display-7">June</th>
                <th class="head-item mbr-fonts-style display-7">July</th>
                <th class="head-item mbr-fonts-style display-7">August</th>
                <th class="head-item mbr-fonts-style display-7">September</th>
                <th class="head-item mbr-fonts-style display-7">October</th>
                <th class="head-item mbr-fonts-style display-7">November</th>
                <th class="head-item mbr-fonts-style display-7">December</th>';
          $textData .= "January, February, March, April, May, June, July, August, 
                        September, October, November, December,";
          break;
      }
      
      $table .= '
            <th class="head-item mbr-fonts-style display-7">Total</th>
          </tr>
        </thead>
        <tbody>';
      $textData .= "Total";
        while($row = mysqli_fetch_array($result)) {
          $table .= '
          <tr><th class="display-7">'.$row['Kiosk ID'].'</th>';
          $textData .= "|".$row['Kiosk ID'].",";
          for ($variable = 0;$variable < $variableLen; $variable++)
          {
            $table .= '<th class="display-7">$'.$row[$variable].'</th>';
            $textData .= $row[$variable].",";
          }
          $table .= '<th class="display-7">$'.$row['Total'].'</th>
          </tr>';
          $textData .= $row['Total'];
        }
      $table .= '
          </tbody>
        </table>';

      $reportName = "Cash_Accepted";
      echo "<div class='form-group col-sm-6'>
              <button class='btn btn-secondary' type='submit'
                  onclick='download_csv(\"".$reportName."\",\"".$textData."\")'>Download CSV</button>
            </div>";
      echo $table;
    }
    
    if ($command == "getReaderReport"){
      $readerMode = $_GET['readerMode'];
      $reportType = $_GET['reportType'];
      $timeSpan = $_GET['timeSpan'];
      $startDate = $_GET['startDate'];
      $endDate = $_GET['endDate'];

      if ($readerMode == "all")
      {
        $sql = "call getAllReadersReport('".$_SESSION['operatorID']."','".$timeSpan."', '".$startDate."', '".$endDate."')";
      }
      else{
        $sql = "call getReaderReportbyMode('".$_SESSION['operatorID']."','".$readerMode."','".$timeSpan."', '".$startDate."', '".$endDate."')";
      }
      $result = mysqli_query($db_link,$sql);
      $_SESSION['reportArray'] = array();
      $textArray = "";
      while($row = mysqli_fetch_array($result)) {
        if ( $reportType == "sales"){
          $entry = array($row['Variable'], $row['Transactions'], $row['CreditsReceived'], $row['label']);
          array_push($_SESSION['reportArray'], $entry);
          $textArray .= "".$row['Variable'].", ".$row['Transactions'].", ".$row['CreditsReceived']."|";
        }
        else if ( $reportType == "usage"){
          $entry = array($row['Variable'], $row['CreditsReceived'], $row['Transactions'], $row['label']);
          array_push($_SESSION['reportArray'], $entry);
          $textArray .= "".$row['Variable'].", ".$row['CreditsReceived'].", ".$row['Transactions']."|";
        }
      }
      $textArray = substr($textArray, 0, -1);

      //echo "buildReport('".$timeSpan."','".$textArray."');";

      $table = "
          <div class=\"form-group col-sm-6\">
            <button class=\"btn btn-secondary\" type=\"submit\" 
            style=\"margin-top:2.5rem;\" onclick=\"toggleReaderReport()\">Load Report</button>
          </div>
          <div class=\"form-group col-sm-6\" >
            <button class=\"btn btn-secondary\" style=\"margin-top:2.5rem;\"
                onclick=\"buildReport('".$timeSpan."','".$textArray."','".$reportType."');\"
                value=\"getReaderReport\" id=\"showButt\">Show Graphic</button>
          </div>";


      // accomodate table here
      $table .= '
      <div class="row">
        <h2>Report Type: '.$reportType.' by '.$timeSpan.' </h2>
        <div class="tableContainer">
          <table class="table">
            <thead>
            <tr class="table-heads "> 
              <th class="head-item mbr-fonts-style display-7">'.$timeSpan.'</th> 
              <th class="head-item mbr-fonts-style display-7">Transactions</th>
              <th class="head-item mbr-fonts-style display-7">Credits Received</th>
            </tr>
            </thead>
            <tbody>';
      $textData .= $timeSpan.", Transactions, CreditsReceived";
      if ( $reportType == "sales"){
        $TransactionIdx = 1;
        $CreditsIdx = 2;
      }
      else if ( $reportType == "usage"){
        $TransactionIdx = 2;
        $CreditsIdx = 1;
      }
      for ($idx = 0; $idx < sizeof($_SESSION['reportArray']); $idx++)
      {
        $table .=
        "<tr>
        <td class='body-item mbr-fonts-style display-7'>".$_SESSION['reportArray'][$idx][3]."</td>
        <td class='body-item mbr-fonts-style display-7'>".$_SESSION['reportArray'][$idx][$TransactionIdx]."</td>
        <td class='body-item mbr-fonts-style display-7'>".$_SESSION['reportArray'][$idx][$CreditsIdx]."</td>
        </tr>";
        $textData .=  "|".$_SESSION['reportArray'][$idx][3].
                        ",".$_SESSION['reportArray'][$idx][$TransactionIdx].
                        ",".$_SESSION['reportArray'][$idx][$CreditsIdx];
      }
      
      $table .=  '</tbody>
          </table>
        </div>
      </div>';

      $reportName = "Reader_".$reportType."_Report";
      echo "<div class='form-group col-sm-6'>
              <button class='btn btn-secondary' type='submit'
                  onclick='download_csv(\"".$reportName."\",\"".$textData."\")'>Download CSV</button>
            </div>";
      echo $table;
    }
    mysqli_close($db_link);
  ?>
</body>
</html>
