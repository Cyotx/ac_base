<?php
// Initialize the session
session_start();
 
// Check if the user is already logged in, if yes then redirect him to welcome page
if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
    header("location: dashboard.php");
    exit;
}
 
// Include config file
require_once './db_config.php';
 
// Define variables and initialize with empty values
$username = $password = "";
$username_err = $password_err = $login_err = "";
 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
 
    // Check if username is empty
    if(empty(trim($_POST["username"]))){
        $username_err = "Please enter username.";
    } else{
        $username = trim($_POST["username"]);
    }
    
    // Check if password is empty
    if(empty(trim($_POST["password"]))){
        $password_err = "Please enter your password.";
    } else{
        $password = trim($_POST["password"]);
    }
    
    // Validate credentials
    if(empty($username_err) && empty($password_err)){
        // Prepare a select statement
        //$sql = "SELECT id, username, password FROM users WHERE username = ?";
        $sql = "call autenthicate_Login('".$username."', '".$password."')";
        
        $result = mysqli_query($link,$sql);

        try {
			if ($row = mysqli_fetch_array($result)){
				/*echo "<div class=\"alert alert-success\">
                <strong>Login Correct!". $row['loginUsername'] . "</strong>
				</div>";*/
				$_SESSION['username'] =  $row['loginUsername'];
				$_SESSION['name'] = $row['FirstName'];
				$_SESSION['operator'] = $row['OperatorName'];
				$_SESSION["loggedin"] = true;

				// Close connection
				mysqli_close($link);

				//header('Location: '.'dashboard.php');
				echo '<meta http-equiv="refresh" content="0; URL=dashboard.php">';
			}
			else{
				// Password is not valid, display a generic error message
				$login_err = "Invalid username or password.";
			}
            
        }
        catch (Exception $e){
			// Password is not valid, display a generic error message
			$login_err = "Invalid username or password.";
        }
    }
    
    
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>American Changer Reporter</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="css/AdminLTE.min.css">
  <link rel="stylesheet" href="css/skins/_all-skins.min.css">
</head>
<body class="hold-transition skin-blue layout-top-nav">
	<div class="wrapper">
		<header class="main-header">
			<nav class="navbar navbar-static-top">
				<div class="container">
					<div class="navbar-header">
						<a class="navbar-brand" href="login.php">
							<img alt="Brand" src="/AmericanChanger/img/amchanger40.png">
						</a>
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
							<i class="fa fa-bars"></i>
						</button>
					</div>
				</div>
			<!-- /.container-fluid -->
			</nav>
		</header>
		<!-- Full Width Column -->
		<div class="content-wrapper">
			<div class="container" style="text-align:center; width: 360px; padding: 20px;">
				<iframe name="alertMessagek"src="" style="zoom:0.60" frameborder="0" height="50" width="99.6%"></iframe>
				<h3>American Changer Reporting System</h3>
				<p>Please fill in your credentials to login.</p>
				<?php 
				if(!empty($login_err)){
					echo '<div class="alert alert-danger">' . $login_err . '</div>';
				}
				?>
				<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
					<div class="form-group">
						<label>Username</label>
						<input type="text" name="username" class="form-control <?php echo (!empty($username_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $username; ?>">
						<span class="invalid-feedback"><?php echo $username_err; ?></span>
					</div>    
					<div class="form-group">
						<label>Password</label>
						<input type="password" name="password" class="form-control <?php echo (!empty($password_err)) ? 'is-invalid' : ''; ?>">
						<span class="invalid-feedback"><?php echo $password_err; ?></span>
					</div>
					<div class="form-group">
						<input type="submit" class="btn btn-primary" value="Login">
					</div>
				</form>
			</div>
			<div class="container" style="text-align:center;">
				<h1> Or </h1>
				<button onClick='location.href = "get_client_receipt.php"' class="btn btn-primary">Get Client Receipt</button>
			</div>
			<!-- /.container -->
		</div>
		<!-- /.content-wrapper -->
		
		<footer class="main-footer">
			<div class="container">
				<div class="pull-right hidden-xs">
					<b>Version</b> 2.0
				</div>
				<strong>American Changer Reporter</strong> All rights reserved.
			</div>
			<!-- /.container -->
		</footer>
	</div>
	<!-- ./wrapper -->


<!-- jQuery 2.2.3 -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="js/LTE/app.min.js"></script>

</body>
</html>
