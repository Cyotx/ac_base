<?php
	session_start();
	$operator = $_SESSION['operator'];
	$user = $_SESSION['username'];
	$name = $_SESSION['name'];
	if (!$user){
		header("Location: index.php");
	}
?>
<!DOCTYPE html>
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>American Changer Reporter</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" href="bootstrap/css/bootstrap.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="css/AdminLTE.css">
	<!-- AdminLTE Skins. Choose a skin from the css/skins
	   folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="css/skins/_all-skins.min.css">

	

</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->

<?php
	if ($operator == "American Changer" || $operator == "Tests Operator"){
		
		echo "<body onload=\"loadOperators2('".$operator."')\" class=\"hold-transition skin-blue layout-top-nav\">";
	}
	else{
		echo "<body onload=\"loadUser2('".$operator."')\" class=\"hold-transition skin-blue layout-top-nav\">";
	}
?>

<div class="wrapper">

  <header class="main-header">
    <nav class="navbar navbar-static-top">
		<div class="container">
			<div class="navbar-header">
			  <a class="navbar-brand" href="dashboard.php">
				<img alt="Brand" src="/AmericanChanger/img/amchanger40.png">
			  </a>
			  <!--<a href="dashboard.php" class="navbar-brand"><b>American</b>Changer</a> -->
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
				<i class="fa fa-bars"></i>
			  </button>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse pull-left" id="navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Reports <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="report_summary.php" data-toggle="tooltip" data-placement="bottom" title="All Sales, Cash and Credit">Sales Summary</a></li>
							<li><a href="report_analysis.php" data-toggle="tooltip" data-placement="bottom" title="Sales Report by Category">Sales Analysis</a></li>
							<li class="divider"></li>
							<li><a href="report_transaction.php" data-toggle="tooltip" data-placement="bottom" title="Last Transactions Registry">Transaction Monitor</a></li>
							<?php
								/*$repType = $_SESSION['repType'];
								if ($repType != 2){
									echo "<li><a href=\"report_card.php\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"All Transactions by Card Number\">
									Transactions by Card</a></li>";
								}*/
							?>
							<li><a href="report_searchtransactions.php" data-toggle="tooltip" data-placement="bottom" title="Search by Card Serial Number or Client Name+Credit Card">Search Transactions</a></li>
							<li class="divider"></li>
							<li><a href="report_totalkiosk.php" data-toggle="tooltip" data-placement="bottom" title="Total Sales By Kiosk">Total Sales By Kiosk</a></li>
							<li><a href="report_totalpkg.php" data-toggle="tooltip" data-placement="bottom" title="Total Sales By Package">Total Sales By Package</a></li>
							<li class="divider"></li>
							<li><a href="report_client_receipt.php" data-toggle="tooltip" data-placement="bottom" title="Get Client Receipt">Get Client Receipt</a></li>
						</ul>
					</li>
					<li> <a href="operations.php" role="button">Operations</a> </li>
					<li> <a href="support.php" role="button">Tech Support</a> </li>
					
				</ul>
				<!--
				<form class="navbar-form navbar-left" role="search">
					<div class="form-group">
						<input type="text" class="form-control" id="navbar-search-input" placeholder="Search">
					</div>
				</form> -->
			</div>
			<!-- /.navbar-collapse -->
			<!-- Navbar Right Menu -->
			<div class="navbar-custom-menu">
				<ul class="nav navbar-nav">
					<li><a href="logout.php">Sign Out</a></li>
				</ul>
			</div>
			<!-- /.navbar-custom-menu -->
		</div>
      <!-- /.container-fluid -->
    </nav>
  </header>
  <!-- Full Width Column -->
	<div class="content-wrapper">
		<div class="container">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
				  Operations
				  <!--<small>Example 2.0</small>-->
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Operations</a></li>
				<!--	<li><a href="#">Layout</a></li>
					<li class="active">Top Navigation</li>-->
				</ol>
			</section>

			<!-- Main content -->
			<section class="content">
				<div class="box box-primary">
					<div class="box-header with-border">
					  <h3 class="box-title">Select Your Search Parameters</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					
					<div class="box-body">
						<div class="col-sm-6">
							<div class="form-group">
								<label class="col-sm-2 control-label">Operator</label>
								<div class="col-sm-10">

									<div id="selectOperator">
									</div>
									
									<?php
										if ($operator != "American Changer" && $operator != "Tests Operator"){
											echo "<select class=\"form-control\" Id=\"operator\">";
											echo "<option value='" .$operator." '>" .$operator. "</option>";
											echo "</select>";
										}
									?>
								</div>
							</div>
							<br></br>
						</div>
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<?php
							$operator = $_SESSION['operator'];
							if ($operator == "American Changer" || $operator == "Tests Operator"){
								echo "  <button type=\"submit\" class=\"btn btn-info pull-right\" data-toggle=\"modal\" data-target=\"#modalAddOperator\">
											Add New Operator
										</button>";
							}
						?>
					</div>
					<!-- /.box-footer -->
					
				</div>
				<!-- /.selection box Machine List-->
				
				<div class="col-sm-6">
					<div class="box box-default">
						<div class="box-header with-border">
							<h3 class="box-title">Machine List</h3>
						</div>
						<div class="box-body">
							<div id="selectInfo"><i>Select an Operator</i>
								
							</div>
						</div>

					</div>
				</div>
				
			<!-- /.box Machine Details-->
				<div class="col-sm-6">
					<div class="row">
						<div class="box box-default">
							<div class="box-header with-border">
								<h3 class="box-title">Machine Details</h3>
							</div>
							<div class="box-body">
								<div class="col-xs-12" id="detailsPanel" style="visibility:hidden">
									<div class="row">
										<label class="col-xs-6" for="kioskName">Machine Name:</label>
										<p id="kioskName" class="col-xs-5"></p>
										<p id="opId" class="col-xs-1" style="visibility:hidden"></p>
									</div>
									<div class="row">
										<label class="col-xs-6" for="kioskSerial">Machine Serial Number:</label>
										<p id="kioskSerial" class="col-xs-5"></p>
										<p id="opFN" class="col-xs-1" style="visibility:hidden"></p>
									</div>
									<div class="row">
										<label class="col-xs-6" for="kioskStatus">Status:</label>
										<p id="kioskStatus" class="col-xs-5"></p>
										<p id="opLN" class="col-xs-1" style="visibility:hidden"></p>
									</div>
									<div class="row">
										<label class="col-xs-6" for="kioskIp">Ip Address:</label>
										<p id="kioskIp" class="col-xs-5"></p>
										<p id="opEmail" class="col-xs-1" style="visibility:hidden"></p>
									</div>
									<div class="row">
										<label class="col-xs-6" for="kioskNote">Notes:</label>
										<p id="kioskNote" class="col-xs-5"></p>
										<p id="opUN" class="col-xs-1" style="visibility:hidden"></p> 
									</div>
									<div class="row">
										<label class="col-xs-6" for="kioskID">Id:</label>
										<p id="kioskID" class="col-xs-5"></p> 
										<p id="opPid" class="col-xs-1" style="visibility:hidden"></p> 
										<p id="opPhone" class="col-xs-1" style="visibility:hidden"></p> 
									</div>
									
									<?php
										$operator = $_SESSION['operator'];
										if ($operator == "American Changer" || $operator == "Tests Operator"){
											echo "<button class=\"btn btn-primary pull-right\" data-toggle=\"modal\" data-target=\"#modalEditKiosk\">Edit Kiosk Data</button>";
											echo "<button class=\"btn btn-danger pull-right\" data-toggle=\"modal\" data-target=\"#modalDeleteKiosk\">Delete Kiosk</button>";
										}
									?>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="box box-default">
							<div class="box-header with-border">
								<h3 class="box-title">Machine Status</h3>
							</div>
							<div class="box-body">
								<div class="col-xs-12" id="noStatusPanel" style="visibility:hidden">
									<div class="row">
										<label class="col-xs-12">No Status info available for this machine</label>
									</div>
								</div>
								<div class="col-xs-12" id="statusPanel" style="visibility:hidden">
								</div>
							</div>
						</div>
					</div>
				</div>
			<!-- /.box -->
			</section>
			<!-- /.content -->
		</div>
		<!-- /.container -->
	</div>
	<!-- /.content-wrapper -->
	<footer class="main-footer">
		<div class="container">
			<div class="pull-right hidden-xs">
				<b>Version</b> 2.0
			</div>
			<strong>American Changer Reporter</strong> All rights reserved.
		</div>
		<!-- /.container -->
	</footer>
</div>
<!-- ./wrapper -->

		<!-- Modals -->
	<div id="modalAddOperator" class="modal fade" role="dialog">
			
		<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Add New Operator</h4>
			</div>
			<div class="modal-body">
				
			<iframe name="alertMessage"src="" style="zoom:0.60" frameborder="0" height="50" width="99.6%"></iframe>
			
			<form method="get" action="saveData.php" target="alertMessage">
				<div class="form-group">
					<label for="opName">Operator Name:</label>
					<input type="text" class="form-control" name="opName" required>
				</div>
				
				<label for="usr">Contact Information</label>
				<div class="form-group">
					<label for="usr">Name:</label>
					<input type="text" class="form-control" name="f_name" required>
				</div>
				
				<div class="form-group">
					<label for="usr">Last Name:</label>
					<input type="text" class="form-control" name="l_name" required>
				</div>
				<div class="form-group">
					<label for="usr">Personal ID Number:</label>
					<input type="text" class="form-control" name="p_id" required>
				</div>
				<div class="form-group">
					<label for="usr">Phone Number:</label>
					<input type="text" class="form-control" name="phone" required>
				</div>
				<div class="form-group">
					<label for="usr">Email:</label>
					<input type="email" class="form-control" name="email" required>
				</div>
				<div class="form-group">
					<label for="reportType">Report Type:</label>
					<label class="radio-inline"><input type="radio" value="1" name="repType">Smart Card</label>
					<label class="radio-inline"><input type="radio" value="2" name="repType">Ticket Kiosk</label>
				</div>
				<div class="form-group">
					<label for="usr">Username:</label>
					<input type="text" class="form-control" name="usr" required>
				</div>
				<div class="form-group">
					<label for="pwd">Password:</label>
					<input name="pwd" id="pwd" class="form-control" type="password" required>
				</div>

				<div class="form-group">
					<label for="conf_pwd">Confirm Password:</label>
					<input name="conf_pwd" id="conf_pwd" onkeyup="checkPass(); return false;" class="form-control" type="password" required>
					<span id="confirmMessage" class="confirmMessage"></span>
				</div>
				
				<button type="submit" value="insertNewOperator" class="btn btn-primary" name="command">Accept</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</form>
			</div>
		</div>
		</div>
	</div>

		<!-- Modals -->
		<div id="modalEditOperator" class="modal fade" role="dialog">
			
			<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Edit Operator</h4>
			  </div>
			  <div class="modal-body">
				  
				<iframe name="alertMessage_OE"src="" style="zoom:0.60" frameborder="0" height="50" width="99.6%"></iframe>
				
				<form method="get" action="saveData.php" target="alertMessage_OE">
					<div class="form-group">
					  <label for="opName_OE">Operator Name:</label>
					  <input type="text" class="form-control" id="opName_OE" name="opName_OE" required>
					</div>
					
					<label for="usr">Contact Information</label>
					<div class="form-group">
					  <label for="usr">Name:</label>
					  <input type="text" class="form-control" id="f_name_OE" name="f_name_OE" required>
					</div>
					
					<div class="form-group">
					  <label for="usr">Last Name:</label>
					  <input type="text" class="form-control" id="l_name_OE" name="l_name_OE" required>
					</div>
					<div class="form-group">
					  <label for="usr">Email:</label>
					  <input type="email" class="form-control" id="email_OE" name="email_OE" required>
					</div>
					<div class="form-group">
					  <label for="usr">Personal ID Number:</label>
					  <input type="text" class="form-control" id="p_id_OE" name="p_id_OE" required>
					</div>
					<div class="form-group">
					  <label for="usr">Phone Number:</label>
					  <input type="text" class="form-control" id="phone_OE" name="phone_OE" required>
					</div>
					<div class="form-group">
					  <label for="reportType">Report Type:</label>
					  <label class="radio-inline"><input type="radio" value="1" id="repType_OE1" name="repType_OE">Smart Card</label>
					  <label class="radio-inline"><input type="radio" value="2" id="repType_OE2" name="repType_OE">Ticket Kiosk</label>
					</div>
					
					<div class="form-group">
					  <label for="usr">Username:</label>
					  <input type="text" class="form-control" id="usr_OE" name="usr_OE" required>
					</div>
					<div class="form-group" style="visibility:hidden">
						<label for="pwd">Password:</label>
						<input name="pwd" id="pwd" class="form-control" type="password">
						<input type="text" class="form-control" id="opID_OE" name="opID_OE">
					</div>
					<div class="form-group" style="visibility:hidden">
						<label for="conf_pwd">Confirm Password:</label>
						<input name="conf_pwd" id="conf_pwd" onkeyup="checkPass(); return false;" class="form-control" type="password">
						<span id="confirmMessage" class="confirmMessage"></span>
					</div>
					
					<button type="submit" value="editOperatorData" class="btn btn-primary" name="command">Accept</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</form>
			  </div>
			</div>
		  </div>
		</div>
		
		<!-- Modals -->
		<div id="modalDeleteOperator" class="modal fade" role="dialog">
			
			<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Delete Operator</h4>
			  </div>
			  <div class="modal-body">
				  
				<iframe name="alertMessage_OD"src="" style="zoom:0.60" frameborder="0" height="50" width="99.6%"></iframe>
				<div>
					<h3 style="color:red"> Are you sure that you want to delete this Operator?</h3>
					<h4> This will also delete ALL DATA related to it (User, Machines, Transactions)</h4>
				</div>
				<form method="get" action="saveData.php" target="alertMessage_OD">
					<div class="form-group">
					  <label for="opName_OD">Operator Name:</label>
					  <input type="text" class="form-control" id="opName_OD" name="opName_OD" readonly>
					</div>
					
					<label for="usr">Contact Information</label>
					<div class="form-group">
					  <label for="usr">Name:</label>
					  <input type="text" class="form-control" id="f_name_OD" name="f_name_OD" readonly>
					</div>
					
					<div class="form-group">
					  <label for="usr">Last Name:</label>
					  <input type="text" class="form-control" id="l_name_OD" name="l_name_OD" readonly>
					</div>
					
					<div class="form-group">
					  <label for="usr">Username:</label>
					  <input type="text" class="form-control" id="usr_OD" name="usr_OD" readonly>
					</div>
					
					<div class="form-group" style="visibility:hidden">
					  <input type="text" class="form-control" id="opID_OD" name="opID_OD" readonly style="visibility:hidden">
					</div>
					
					<button type="submit" value="deleteOperator" class="btn btn-primary" name="command">Accept</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</form>
			  </div>
			</div>
		  </div>
		</div>
		
		<!-- Modals KIOSK -->
		<div id="modalAddKiosk" class="modal fade" role="dialog">
		  <div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add New Machine</h4>
			  </div>
			  <div class="modal-body">
				  
				<iframe name="alertMessagek"src="" style="zoom:0.60" frameborder="0" height="50" width="99.6%"></iframe>
				
				<form method="get" action="saveData.php" target="alertMessagek">
					
					
					<div class="form-group">
					  <label for="opName">Operator Name:</label>
					  <input type="text" value="Operator" class="form-control" id="opName_add" name="opName_add" readonly>
					</div>
					
					<div class="form-group">
					  <label for="kNameForm">Machine Name:</label>
					  <input type="text" class="form-control" name="kNameForm" required>
					</div>
					
					<div class="form-group">
					  <label for="kSerialForm">Serial Number:</label>
					  <input type="text" class="form-control" name="kSerialForm">
					</div>
					
					<div class="form-group">
					  <label for="kIpForm">Ip Address:</label>
					  <input type="text" class="form-control" name="kIpForm" required>
					</div>
					
					<div class="form-group">
					  <label for="kMachineTypeForm">Machine Type:</label>
					  <label class="radio-inline"><input type="radio" value="1" name="kMachineTypeForm">Kiosk</label>
					  <label class="radio-inline"><input type="radio" value="2" name="kMachineTypeForm">Card Dispenser</label>
					  <label class="radio-inline"><input type="radio" value="3" name="kMachineTypeForm">Glory</label>
					</div>
					
					<div class="form-group">
					  <label for="kNotesForm">Notes:</label>
					  <textarea type="text" class="form-control" name="kNotesForm"></textarea>
					</div>
					<button type="submit" value="insertNewKiosk" class="btn btn-primary" name="command">Accept</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</form>
				  
			  </div>
			</div>

		  </div>
		</div>
		
		<!-- Edit Kiosk -->
		<div id="modalEditKiosk" class="modal fade" role="dialog">
		  <div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Edit Machine Data</h4>
			  </div>
			  <div class="modal-body">
				  
				<iframe name="alertMessagef"src="" style="zoom:0.60" frameborder="0" height="50" width="99.6%"></iframe>
				
				<form method="get" action="saveData.php" target="alertMessagef">
					
					<div class="form-group">
					  <label for="opNamek">Operator Name:</label>
					  <input type="text" value="Operator" class="form-control" id="opNamek" name="opNamek" readonly>
					</div>

					<div class="form-group">
					  <label for="kNameForm">Machine Name:</label>
					  <input type="text" class="form-control" id="kNameForm" name="kNameForm" required>
					</div>
					
					<div class="form-group">
					  <label for="kSerialForm">Serial Number:</label>
					  <input type="text" class="form-control" id="kSerialForm" name="kSerialForm">
					</div>
					
					<div class="form-group">
					  <label for="kIpForm">Ip Address:</label>
					  <input type="text" class="form-control" id="kIpForm" name="kIpForm" required>
					</div>
					
					<div class="form-group">
					  <label for="kMachineTypeForm">Machine Type:</label>
					  <label class="radio-inline"><input type="radio" value="1" name="kMachineTypeForm">Kiosk</label>
					  <label class="radio-inline"><input type="radio" value="2" name="kMachineTypeForm">Card Dispenser</label>
					  <label class="radio-inline"><input type="radio" value="3" name="kMachineTypeForm">Glory</label>
					</div>
					
					<div class="form-group">
					  <label for="kNotesForm">Notes:</label>
					  <textarea type="text" class="form-control" id="kNotesForm" name="kNotesForm"></textarea>
					</div>
					<div class="form-group" style="visibility:hidden">
					  <label for="kioskId" style="visibility:hidden">Kiosk Id:</label>
					  <input type="text" value="Operator" class="form-control" id="kioskId" name="kioskId" readonly style="visibility:hidden">
					</div>
					<button type="submit" value="editKioskData" class="btn btn-primary" name="command" Location.reload>Accept</button>
					
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</form>
				  
			  </div>
			</div>

		  </div>
		</div>
		
		<!-- Modals  -->
		<div id="modalDeleteKiosk" class="modal fade" role="dialog">
		  <div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Delete Machine</h4>
			  </div>
			  <div class="modal-body">
				  
				<iframe name="alertMessager"src="" style="zoom:0.60" frameborder="0" height="50" width="99.6%"></iframe>
				
				<form method="get" action="saveData.php" target="alertMessager">
					<div>
						<h3 style="color:red"> Are you sure that you want to delete this Machine?</h3>
						<h4> This will also delete ALL TRANSACTIONS DATA related to it...</h4>
					</div>
					<div class="form-group">
					  <label for="opNamek">Operator Name:</label>
					  <input type="text" value="Operator" class="form-control" id="opNamek_delete" name="opNamek" readonly>
					</div>
					
					<div class="form-group">
					  <label for="kNameForm">Machine Name:</label>
					  <input type="text" class="form-control" id="kNameForm_delete" name="kNameForm" readonly>
					</div>
					
					<div class="form-group">
					  <label for="kIpForm">Ip Address:</label>
					  <input type="text" class="form-control" id="kIpForm_delete" name="kIpForm" readonly>
					</div>
					
					<div class="form-group" style="visibility:hidden">
					  <label for="kioskId" style="visibility:hidden">Machine Id:</label>
					  <input type="text" value="Operator" class="form-control" id="kioskId_delete" name="kioskId_delete" readonly style="visibility:hidden">
					</div>
					<button type="submit" value="deleteKiosk" class="btn btn-primary" name="command">Delete</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</form>
				  
			  </div>
			</div>

		  </div>
		</div>





<!-- jQuery 2.2.3 -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="js/LTE/app.min.js"></script>
<!-- Local Operations -->
<script type="text/javascript" src="js/operations.js"></script>
<script type="text/javascript" src="js/jquery.canvasjs.min.js"></script>

</body>
</html>
