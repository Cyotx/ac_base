/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
 
 
function loadSummaryChart(titulos,dps){
	
	var dpsSummary = [];
	for (i = 0; i < 7; i++){
		dpsSummary.push({x: i*10, y: dps[i],label:titulos[i]});
	}
	dpsSummary.push({x: 7, y: ""});
	var chart = new CanvasJS.Chart("chartContainer",
		{
			animationEnabled: true,
			title:{
				text: "Total Sales"
			},
			axisX: {
				interval:10,
				title: "Concept",
						
		    },
			axisY:{
				title: "Total Sales ($)",
				includeZero: false,
				minimum:0
			},
			data: [
			{
				type: "column", //change type to bar, line, area, pie, etc
				dataPoints: dpsSummary
			}
			]
		});
		chart.render();
}

function loadAnalysisChart(pkg, wb ,titles, lenght, chartStr, concept){
	var wbSummary = [];
	var pkgSummary = [];
	for (i = 0; i < lenght; i++){
		wbSummary.push({x: i*10, y: wb[i],label:titles[i]});
		pkgSummary.push({x: i*10, y: pkg[i],label:titles[i]});
	}
	//dpsSummary.push({x: lenght*10, y: ""});
	
	//income = [{x: 1, y: 1},{x: 2, y: 2},{x: 3, y: 3},{x: 4, y: 4}]
	var chart = new CanvasJS.Chart("wbContainer",
	{
		animationEnabled: true,
		title:{
			text: chartStr + " (Wristbands)"
		},
		axisX: {
			interval:10,
			title: concept,
			labelAngle: -50
		},
		axisY:{
			title: "Total Sales ($)",
			includeZero: false,
			minimum:0
		},
		data: [
		{
			type: "column", //change type to bar, line, area, pie, etc
			dataPoints: wbSummary
		}
		]
	});
	
	var chart2 = new CanvasJS.Chart("pkgContainer",
	{
		animationEnabled: true,
		title:{
			text: chartStr + " (Packages)"
		},
		axisX: {
			interval:10,
			title: concept,
			labelAngle: -50
		},
		axisY:{
			title: "Total Sales ($)",
			includeZero: false,
			minimum:0
		},
		data: [
		{
			type: "column", //change type to bar, line, area, pie, etc
			dataPoints: pkgSummary
		}
		]
	});
	
	chart.render();
	chart2.render();
}
 
function loadGraphicData(type, payType){
	if (type == "Summary"){
		var dpp = document.getElementById('div1').innerHTML;
		
		var lines = dpp.split("-");
		var income = [];
		var titulos = [];
		for (i = 0; i < 7; i++) {
			var title = lines[i].split(":")[0];
			valor1 = title.split("/");
			titulos.push(valor1[1]);
			var valores = lines[i].split(":")[1];
			valor = valores.split("/");
			income.push(parseInt(valor[1]));
		}
		loadSummaryChart(titulos,income);
	}
	
	else{
		
		if (payType=='1'){
			var chartStr = "Cash Sales by ";
		}
		if (payType=='2'){
			var chartStr = "Credit Card Sales by ";
		}
		if (payType=='3'){
			var chartStr = "Total Sales by ";
		}
		
		if (type == "1"){
			var largo = 24;
			var concept = "Hour";
			chartStr += concept;
		}
		if (type == "2"){
			var largo = 30;
			var concept = "Date";
			chartStr += concept;
		}
		if (type == "3"){
			var largo = 7;
			var concept = "Weekday";
			chartStr += concept;
		}
		if (type == "4"){
			var largo = 12;
			var concept = "Month";
			chartStr += concept;
		}

		var dpp = document.getElementById('div1').innerHTML;
		
		var lines = dpp.split("*");
		
		var incomePkg = [];
		var incomeWB = [];
		
		var titulos = [];
		
		for (i = 0; i < largo; i++) {
			var title = lines[i].split("!")[0];
			valor1 = title.split("/");
			titulos.push(valor1[1]);
			var valores = lines[i].split("!")[1];
			valor = valores.split("/");
			incomePkg.push(parseInt(valor[0]));
			incomeWB.push(parseInt(valor[1]));
		}
		loadAnalysisChart(incomePkg, incomeWB ,titulos, largo, chartStr, concept);
		
	}
	
}

function loadAnalysisCanvas() {
	

	document.getElementById("ReportAnalysis").innerHTML = " <div align=\"center\"><h1>LOADING...</h1><div class=\"loader\"></div></div> ";
	document.getElementById("report_table").style.visibility = "visible";
	
	var dateRange = document.getElementById("datetimeRange").innerHTML;
	var dateList = dateRange.split(" ");
	var dateLow = dateList[0] + " " + dateList[1];
	var dateHigh = dateList[3] + " " + dateList[4];
	
	var opIndex = document.getElementById("operator").selectedIndex;
	var opOptions = document.getElementById("operator").options;
	var op = opOptions[opIndex].text;
	
	var kioskIndex = document.getElementById("k_name").selectedIndex;
	var kioskOptions = document.getElementById("k_name").options;
	var kiosk = kioskOptions[kioskIndex].text;
	
	var typeIndex = document.getElementById("report_type").selectedIndex;
	var typeOptions = document.getElementById("report_type").options;
	var type = typeOptions[typeIndex].value;
	
	var credit=document.getElementById('credit');
	var cash=document.getElementById('cash');
	var payType='3';
	document.getElementById("reportTitle").innerHTML = "Total Sales Analysis Report";
	if (cash.checked && credit.checked) {
		payType='3';
	}
	else{
		if (cash.checked){
			payType='1';
			document.getElementById("reportTitle").innerHTML = "Cash Analysis Report";
		}
		if (credit.checked){
			payType='2';
			document.getElementById("reportTitle").innerHTML = "Credit Card Analysis Report";
		}
	}
	
	var complete=document.getElementById('complete');
	var incomplete=document.getElementById('incomplete');
	var SalesType='3';
	if (complete.checked && incomplete.checked) {
		SalesType='3';
	}
	else{
		if (complete.checked){
			SalesType='1';
		}
		if (incomplete.checked){ 
			SalesType='2';
		}
	}

	if (kiosk == "") {
		document.getElementById("ReportAnalysis").innerHTML = "";
		return;
	} else {
		if (window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		} else {
			// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				document.getElementById("ReportAnalysis").innerHTML = xmlhttp.responseText;
			}
		};
		//xmlhttp.open("GET","getTest.php?command=loadPackageData&q="+str,true);
		xmlhttp.open("GET","getTest.php?command=loadAnalysis&Type='"+type+"'&Op='"+op+"'&Kiosk='"+kiosk+"'&PayType='"+payType+"'&SalesType='"+SalesType+"'&DateLow='"+dateLow+"'&DateHigh='"+dateHigh+"'",true);
		xmlhttp.send();
	}
	
	// Graficar datos diferentes?? puede que sirva la misma funcion
	//alert(document.getElementById("div1").innerHTML);
	document.getElementById("pkgContainer").innerHTML = "<div class= \"row\"><h2 class=\"col-xs-6 pull-left\" style=\"text-align:left\">Scroll Down to see the Report Table</h2> </div> <br> <div class =\"row\"><button class=\"btn btn-info pull-left\" style=\"height:100px\" onclick=\"loadGraphicData("+type+","+payType+");\">Press to View Charts</button> </div> ";
	document.getElementById("wbContainer").innerHTML = "";
}


function loadSummaryCanvas() {
	
	document.getElementById("ReportSummary").innerHTML = " <div align=\"center\"><h1>LOADING...</h1><div class=\"loader\"></div></div> ";
	
	var dateRange = document.getElementById("datetimeRange").innerHTML;
	var dateList = dateRange.split(" ");
	var dateLow = dateList[0] + " " + dateList[1];
	var dateHigh = dateList[3] + " " + dateList[4];
	document.getElementById("report_table").style.visibility = "visible";

	var opIndex = document.getElementById("operator").selectedIndex;
	var opOptions = document.getElementById("operator").options;
	var op = opOptions[opIndex].text;
	
	var kioskIndex = document.getElementById("k_name").selectedIndex;
	var kioskOptions = document.getElementById("k_name").options;
	var kiosk = kioskOptions[kioskIndex].text;
	
	var complete=document.getElementById('complete');
	var incomplete=document.getElementById('incomplete');
	var SalesType='3';
	if (complete.checked && incomplete.checked) {
		SalesType='3';
	}
	else{
		if (complete.checked){
			SalesType='1';
		}
		if (incomplete.checked){ 
			SalesType='2';
		}
	}
	
	if (op == "Select an Operator") {
		document.getElementById("ReportSummary").innerHTML = "Select an Operator";
		return;
	}
	
	if (kiosk == "Kiosk Name") {
		document.getElementById("chartContainer").innerHTML = "Select a Kiosk";
		return;
	} else {
		if (window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		} else {autoFillInfo
			// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				document.getElementById("ReportSummary").innerHTML = xmlhttp.responseText;
			}
		};
		//xmlhttp.open("GET","getTest.php?command=loadPackageData&q="+str,true);
		xmlhttp.open("GET","getTest.php?command=loadPackageData&Op='"+op+"'&Kiosk='"+kiosk+"'&SalesType='"+SalesType+"'&DateLow='"+dateLow+"'&DateHigh='"+dateHigh+"'",true);
		xmlhttp.send();
	}
	//alert("Loading Chart Data");
	//alert(document.getElementById("div1").innerHTML);
	
	document.getElementById("chartContainer").innerHTML = "<div class= \"row\"><h2 class=\"col-xs-6 pull-left\" style=\"text-align:left\">Scroll Down to see the Report Table</h2> </div> <br> <div class =\"row\"> <button class=\"btn btn-info \" style=\"height:100px\" onclick=\"loadGraphicData('Summary','0')\">Press to View Chart</button> </div>";
}

function loadTransactionCanvas() {
	
	document.getElementById("ReportTransaction").innerHTML = " <div align=\"center\"><h1>LOADING...</h1><div class=\"loader\"></div></div> ";
	
	document.getElementById("report_table").style.visibility = "visible";
	
	var dateRange = document.getElementById("datetimeRange").innerHTML;
	var dateList = dateRange.split(" ");
	var dateLow = dateList[0] + " " + dateList[1];
	var dateHigh = dateList[3] + " " + dateList[4];
	var opIndex = document.getElementById("operator").selectedIndex;
	var opOptions = document.getElementById("operator").options;
	var op = opOptions[opIndex].text;
	
	var kioskIndex = document.getElementById("k_name").selectedIndex;
	var kioskOptions = document.getElementById("k_name").options;
	var kiosk = kioskOptions[kioskIndex].text;
	
	var lastIndex = document.getElementById("transaction_qty").selectedIndex;
	var lastOptions = document.getElementById("transaction_qty").options;
	var last = lastOptions[lastIndex].value;
	
	var credit=document.getElementById('credit');
	var cash=document.getElementById('cash');
	var payType='3';
	if (cash.checked && credit.checked) {
		payType='3';
	}
	else{
		if (cash.checked){
			payType='1';
		}
		if (credit.checked){
			payType='2';
		}
	}
		
	
	if (kiosk == "") {
		document.getElementById("ReportTransaction").innerHTML = "Select and Operator and a Kiosk";
		return;
	} else {
		if (window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		} else {
			// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				document.getElementById("ReportTransaction").innerHTML = xmlhttp.responseText;
			}
		};
		//xmlhttp.open("GET","getTest.php?command=loadPackageData&q="+str,true);
		xmlhttp.open("GET","getTest.php?command=loadTransactions&Op='"+op+"'&Kiosk='"+kiosk+"'&PayType='"+payType+"'&Qty="+last+"&DateLow='"+dateLow+"'&DateHigh='"+dateHigh+"'",true);
		xmlhttp.send();
	}
	
}

function searchReceipts() {
	
	document.getElementById("ReceiptList").innerHTML = " <div align=\"center\"><h1>LOADING...</h1><div class=\"loader\"></div></div> ";
	document.getElementById("report_table").style.visibility = "visible";
	
	var lastname = document.getElementById("lastname").value;
	var cardNo = document.getElementById("CardNo").value;
	

	if (window.XMLHttpRequest) {
		// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {
		// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			document.getElementById("ReceiptList").innerHTML = xmlhttp.responseText;
		}
	};
	//xmlhttp.open("GET","getTest.php?command=loadPackageData&q="+str,true);
	//alert ("getTest.php?command=searchReceipts&lastname='"+lastname+"'&CardNo='"+cardNo+"'");
	xmlhttp.open("GET","getTest.php?command=searchReceipts&lastname='"+lastname+"'&CardNo='"+cardNo+"'",true);
	xmlhttp.send();
}


function loadTotalKioskCanvas() {
	
	document.getElementById("ReportTransaction").innerHTML = " <div align=\"center\"><h1>LOADING...</h1><div class=\"loader\"></div></div> ";
	document.getElementById("report_table").style.visibility = "visible";
	
	var dateRange = document.getElementById("datetimeRange").innerHTML;
	var dateList = dateRange.split(" ");
	var dateLow = dateList[0] + " " + dateList[1];
	var dateHigh = dateList[3] + " " + dateList[4];
	var opIndex = document.getElementById("operator").selectedIndex;
	var opOptions = document.getElementById("operator").options;
	var op = opOptions[opIndex].text;
	
	var kioskIndex = document.getElementById("k_name").selectedIndex;
	var kioskOptions = document.getElementById("k_name").options;
	var kiosk = kioskOptions[kioskIndex].text;
	
	var complete=document.getElementById('complete');
	var incomplete=document.getElementById('incomplete');
	var salesToReport='3';
	if (complete.checked && incomplete.checked) {
		salesToReport='3';
	}
	else{
		if (complete.checked){
			salesToReport='1';
		}
		if (incomplete.checked){
			salesToReport='2';
		}
	}
	//alert(salesToReport);
	if (kiosk == "Kiosk Name") {
		alert("Please select a Kiosk");
		document.getElementById("ReportTransaction").innerHTML = "Select an Operator and a Kiosk";
		return 1;
	} else {
		if (window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		} else {
			// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				document.getElementById("ReportTransaction").innerHTML = xmlhttp.responseText;
			}
		};
		//xmlhttp.open("GET","getTest.php?command=loadPackageData&q="+str,true);
		xmlhttp.open("GET","getTest.php?command=loadTotalKiosk&Op='"+op+"'&Kiosk='"+kiosk+"'&DateLow='"+dateLow+"'&DateHigh='"+dateHigh+"'&SalesToReport='"+salesToReport+"'",true);
		xmlhttp.send();
	}
}

function loadTotalPkgCanvas() {
	
	document.getElementById("ReportTransaction").innerHTML = " <div align=\"center\"><h1>LOADING...</h1><div class=\"loader\"></div></div> ";
	document.getElementById("report_table").style.visibility = "visible";
	
	var dateRange = document.getElementById("datetimeRange").innerHTML;
	var dateList = dateRange.split(" ");
	var dateLow = dateList[0] + " " + dateList[1];
	var dateHigh = dateList[3] + " " + dateList[4];
	var opIndex = document.getElementById("operator").selectedIndex;
	var opOptions = document.getElementById("operator").options;
	var op = opOptions[opIndex].text;
	
	var kioskIndex = document.getElementById("k_name").selectedIndex;
	var kioskOptions = document.getElementById("k_name").options;
	var kiosk = kioskOptions[kioskIndex].text;
	
	var complete=document.getElementById('complete');
	var incomplete=document.getElementById('incomplete');
	var SalesType='3';
	if (complete.checked && incomplete.checked) {
		SalesType='3';
	}
	else{
		if (complete.checked){
			SalesType='1';
		}
		if (incomplete.checked){
			SalesType='2';
		}
	}
	
	if (kiosk == "") {
		document.getElementById("ReportTransaction").innerHTML = "Select and Operator and a Kiosk";
		return;
	} else {
		if (window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		} else {
			// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				document.getElementById("ReportTransaction").innerHTML = xmlhttp.responseText;
			}
		};
		//xmlhttp.open("GET","getTest.php?command=loadPackageData&q="+str,true);
		xmlhttp.open("GET","getTest.php?command=loadTotalPkg&Op='"+op+"'&Kiosk='"+kiosk+"'&SalesType='"+SalesType+"'&DateLow='"+dateLow+"'&DateHigh='"+dateHigh+"'",true);
		xmlhttp.send();
	}
}



function searchByClient() {
	

	document.getElementById("ReportTransaction").innerHTML = " <div align=\"center\"><h1>LOADING...</h1><div class=\"loader\"></div></div> ";
	document.getElementById("report_table").style.visibility = "visible";
	
	var opIndex = document.getElementById("operator").selectedIndex;
	var opOptions = document.getElementById("operator").options;
	var op = opOptions[opIndex].text;

	var kioskIndex = document.getElementById("k_name").selectedIndex;
	var kioskOptions = document.getElementById("k_name").options;
	var kiosk = kioskOptions[kioskIndex].text;
	
	var lastname = document.getElementById("lastNameBox").value;
	var creditcard = document.getElementById("creditCardBox").value;
		
	if (kiosk == "") {
		document.getElementById("ReportTransaction").innerHTML = "Select and Operator and a Kiosk";
		return;
	} else {
		if (window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		} else {
			// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				document.getElementById("ReportTransaction").innerHTML = xmlhttp.responseText;
			}
		};
		//xmlhttp.open("GET","getTest.php?command=loadPackageData&q="+str,true);
		xmlhttp.open("GET","getTest.php?command=searchTransactionsByClient&Op='"+op+"'&Kiosk='"+kiosk+"'&Lastname='"+lastname+"'&Creditcard='"+creditcard+"'",true);
		xmlhttp.send();
	}
	
}

function searchTransactions() {
	
	document.getElementById("ReportTransaction").innerHTML = " <div align=\"center\"><h1>LOADING...</h1><div class=\"loader\"></div></div> ";
	document.getElementById("report_table").style.visibility = "visible";
	
	var opIndex = document.getElementById("operator").selectedIndex;
	var opOptions = document.getElementById("operator").options;
	var op = opOptions[opIndex].text;
	
	var kioskIndex = document.getElementById("k_name").selectedIndex;
	var kioskOptions = document.getElementById("k_name").options;
	var kiosk = kioskOptions[kioskIndex].text;
	
	var search = document.getElementById("searchBox").value;
		
	
	if (kiosk == "") {
		document.getElementById("ReportTransaction").innerHTML = "Select and Operator and a Kiosk";
		return;
	} else {
		if (window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		} else {
			// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				document.getElementById("ReportTransaction").innerHTML = xmlhttp.responseText;
			}
		};
		//xmlhttp.open("GET","getTest.php?command=loadPackageData&q="+str,true);
		xmlhttp.open("GET","getTest.php?command=searchTransactions&Op='"+op+"'&Kiosk='"+kiosk+"'&Search='"+search+"'",true);
		xmlhttp.send();
	}
	
}

function searchCards() {
	
	document.getElementById("ReportTransaction").innerHTML = " <div align=\"center\"><h1>LOADING...</h1><div class=\"loader\"></div></div> ";
	document.getElementById("report_table").style.visibility = "visible";
	
	var opIndex = document.getElementById("operator").selectedIndex;
	var opOptions = document.getElementById("operator").options;
	var op = opOptions[opIndex].text;

	var search = document.getElementById("searchBox").value;
	
	if (op == "Select an Operator") {
		document.getElementById("ReportTransaction").innerHTML = "Select an Operator";
		return;
	} else {
		if (window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		} else {
			// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				document.getElementById("ReportTransaction").innerHTML = xmlhttp.responseText;
			}
		};
		//xmlhttp.open("GET","getTest.php?command=loadPackageData&q="+str,true);
		xmlhttp.open("GET","getTest.php?command=searchCards&Op='"+op+"'&Search='"+search+"'",true);
		xmlhttp.send();
	}
	
}



function loadUser(operator,page) {
	if (operator == "") {
		document.getElementById("selectInfo").innerHTML = "";
		return;
	} else {
		if (window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		} else {
			// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				document.getElementById("selectInfo").innerHTML = xmlhttp.responseText;
			}
		};
		xmlhttp.open("GET","getTest.php?command=autoFillInfo&q="+operator+"&page="+page,true);
		xmlhttp.send();
	}
}

function loadOperators(currentOp, page) {
	
	if (window.XMLHttpRequest) {
		// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {
		// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			document.getElementById("selectOperator").innerHTML = xmlhttp.responseText;
		}
	};
	xmlhttp.open("GET","getTest.php?command=getOperatorList&Operator="+currentOp+"&page="+page,true);
	xmlhttp.send();
}


