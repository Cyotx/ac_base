
function downloadPDF(dataString, dateTime) {
	
	dataArray = dataString.split("|");
	
	//pdf.save('hello_world.pdf');
	//pdf.autoPrint()
	
	try {
		var doc = new jsPDF();

		doc.setFontSize(14);
		doc.text(20, 20, "E.K. Fernandez Shows");
		doc.setFontSize(12);
	
		doc.text(20, 40, dataArray[1]);doc.text(100, 40, dataArray[0]);
		doc.text(40, 50, dataArray[2]);
		doc.text(30, 60, dataArray[3]);
		doc.text(30, 70, dataArray[4]);
		doc.text(30, 80, dataArray[5]);
	
		doc.text(30, 90, dataArray[6]);
		doc.text(30, 100, dataArray[7]);
		doc.text(30, 110, dataArray[8]);
	
		doc.text(30, 120, dataArray[9]);
		doc.text(30, 130, dataArray[10]);
		doc.text(30, 140, dataArray[11]);
	
		doc.text(40, 155, dataArray[12]);
	
		var posY = 170;
		var indx = 13;
		while (true)
		{
			try {
				doc.text(30, posY, dataArray[indx]);
				posY += 10;
				indx += 1;
			}
			catch(err) {
				break;
			}
		}
		
		doc.save("Receipt "+ dateTime+'.pdf');
	}
	catch(err) {
		var doc = new jsPDF();

		doc.setFontSize(14);
		doc.text(20, 20, "E.K. Fernandez Shows");
		doc.setFontSize(12);
		
		doc.text(20, 40, "We are very sorry!");
		doc.text(40, 50, "The receipt data for this transaction is not available");
		
		doc.save("Receipt "+ dateTime+'.pdf');
	}
	
	
	//doc.text(40, 170, dataArray.length);
	//doc.addImage(imgData, 'JPEG', 15, 40, 180, 180);

	// Set the document to automatically print via JS
	
	
	/*
	if (window.XMLHttpRequest) {
		// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {
		// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			document.getElementById("selectOperator").innerHTML = xmlhttp.responseText;
		}
	};
	xmlhttp.open("GET","getTest.php?command=getOperatorList&Operator="+currentOp+"&page="+page,true);
	xmlhttp.send();*/
}
