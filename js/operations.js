
function showDetailsPanel(Name, ipAddress, serialNumber, notes, 
	id, idop, firstname, lastname, email, username, phone,
	softVers, system, carwash, cardDisp1, cardDisp2, cardDisp3, cardDisp4,
	tickDisp1, tickDisp2, tickDisp3, tickDisp4, hopp1, hopp2, hopp3, 
	hopp4, val1, val2, coinAcc, ccSyst, billDisp, billCst1, billCst2, 
	billCst3, billCst4, billReject, billMotor, billQual, billDiv, 
	billStack, billPres, billData, display, program, printer1, printer2,
	temp, volt, door, datetime){

	//document.getElementById('detailsTitle').innerHTML = "Kiosk Details";
	document.getElementById('kioskName').innerHTML = Name;
	document.getElementById('kioskSerial').innerHTML = serialNumber;
	document.getElementById('kioskStatus').innerHTML = "Active";
	document.getElementById('kioskIp').innerHTML = ipAddress;
	//
	document.getElementById('kioskNote').innerHTML = notes;
	document.getElementById('detailsPanel').style.visibility = "visible";
	document.getElementById('kioskID').innerHTML = id;
	opDetailsPanel(idop, firstname, lastname, email,username, phone);
	// Update Status panel
	machineStatusPanel(softVers, system, carwash, cardDisp1, cardDisp2, cardDisp3, cardDisp4,
					   tickDisp1, tickDisp2, tickDisp3, tickDisp4, hopp1, hopp2, hopp3, 
					   hopp4, val1, val2, coinAcc, ccSyst, billDisp, billCst1, billCst2, 
					   billCst3, billCst4, billReject, billMotor, billQual, billDiv, 
					   billStack, billPres, billData, display, program, printer1, printer2,
					   temp, volt, door, datetime);
}

function opDetailsPanel(id, firstname, lastname, email,username, phone){
	document.getElementById('opId').innerHTML = id;
	document.getElementById('opFN').innerHTML = firstname;
	document.getElementById('opLN').innerHTML = lastname;
	document.getElementById('opEmail').innerHTML = email;
	document.getElementById('opUN').innerHTML = username;
	document.getElementById('opPhone').innerHTML = phone;
}

function machineStatusPanel(softVers, system, carwash, cardDisp1, cardDisp2, cardDisp3, cardDisp4,
	tickDisp1, tickDisp2, tickDisp3, tickDisp4, hopp1, hopp2, hopp3, hopp4, val1, val2, coinAcc, ccSyst,
	billDisp, billCst1, billCst2, billCst3, billCst4, billReject, billMotor, billQual, billDiv, billStack,
	billPres, billData, display, program, printer1, printer2, temp, volt, door, datetime){
	
	
	if (softVers == '0'){
		document.getElementById('noStatusPanel').style.visibility = "visible";
		document.getElementById('statusPanel').style.visibility = "hidden";
	}
	
	else{
		document.getElementById('noStatusPanel').style.visibility = "hidden";
		document.getElementById('statusPanel').style.visibility = "visible";

		var headers = ["Latest Update","Software Version","System", "Car Wash","Card Dispenser 1","Card Dispenser 2","Card Dispenser 3","Card Dispenser 4",
					   "Ticket Dispenser 1","Ticket Dispenser 2","Ticket Dispenser 3","Ticket Dispenser 4","Hopper 1","Hopper 2","Hopper 3",
					   "Hopper 4","Validator 1","Validator 2","Coin Acceptor","Credit Card System","Bill Dispenser","Bill Cassette 1","Bill Cassette 2",
					   "Bill Cassette 3","Bill Cassette 4","Bill Dispenser Reject","Bill Dispenser Motor","Bill Dispenser Qualifier","Bill Dispenser Diverter",
					   "Bill Dispenser Stacker","Bill Dspenser Presenter","Bill Dispenser Data","Display","Programmer","Printer 1","Printer 2","Temperature","Voltage","Door"];
		var info = [datetime, softVers, system, carwash, cardDisp1, cardDisp2, cardDisp3, cardDisp4,
					tickDisp1, tickDisp2, tickDisp3, tickDisp4, hopp1, hopp2, hopp3, 
					hopp4, val1, val2, coinAcc, ccSyst, billDisp, billCst1, billCst2, 
					billCst3, billCst4, billReject, billMotor, billQual, billDiv, 
					billStack, billPres, billData, display, program, printer1, printer2, temp, volt, door]
		
		var innerText = "";
		
		for (line = 0; line < headers.length; line++) {
			if (info[line] !="?"){
				innerText += "<div class=\"row\"><label class=\"col-xs-6\">"+headers[line]+
							 ":</label><p class=\"col-xs-5\">"+info[line]+"</p></div>";
			}
		}
		
		
		document.getElementById('statusPanel').innerHTML = innerText;
		
		
		/*
		innerText += "<div class=\"row\"><label class=\"col-xs-6\">"+headers[0]+":</label><p class=\"col-xs-5\">"+softVers+"</p></div>";
		innerText += "<div class=\"row\"><label class=\"col-xs-6\">"+headers[1]+":</label><p class=\"col-xs-5\">"+system+"</p></div>";

		document.getElementById('softVers').innerHTML = softVers;
		document.getElementById('system').innerHTML = system;
		document.getElementById('cardDisp1').innerHTML = cardDisp1;
		document.getElementById('cardDisp2').innerHTML = cardDisp2;
		document.getElementById('cardDisp3').innerHTML = cardDisp3;
		document.getElementById('cardDisp4').innerHTML = cardDisp4;
		document.getElementById('hopp1').innerHTML = hopp1;
		document.getElementById('hopp2').innerHTML = hopp2;
		document.getElementById('val1').innerHTML = val1;
		document.getElementById('val2').innerHTML = val2;
		document.getElementById('coinAcc').innerHTML = coinAcc;
		document.getElementById('ccSyst').innerHTML = ccSyst;
		document.getElementById('billDisp').innerHTML = billDisp;
		document.getElementById('display').innerHTML = display;
		document.getElementById('printer').innerHTML = printer;
		document.getElementById('temp').innerHTML = temp;*/
	}
}
/*
function selectPanel(){
	var e = document.getElementById('machine_select');
	var strUser = e.options[e.selectedIndex].value;
	showDetailsPanel(strUser);
}*/

function loadUser2(str) {
	document.getElementById("detailsPanel").style.visibility = "hidden";
	if (str == "") {
		document.getElementById("selectInfo").innerHTML = "";
		return;
	} else {
		if (window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		} else {
			// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				document.getElementById("selectInfo").innerHTML = xmlhttp.responseText;
			}
		};
		xmlhttp.open("GET","getTest.php?command=fillInfoOperations&q="+str,true);
		xmlhttp.send();
	}
}



function loadOperators2(currentOp) {
	if (window.XMLHttpRequest) {
		// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {
		// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			document.getElementById("selectOperator").innerHTML = xmlhttp.responseText;
		}
	};
	
	xmlhttp.open("GET","getTest.php?command=getOperatorList2&Operator="+currentOp,true);
	xmlhttp.send();

	
// onLoad Function
	
	$('#modalEditKiosk').on('shown.bs.modal', function() {
		var opIndex = document.getElementById("operator").selectedIndex;
		var opOptions = document.getElementById("operator").options;
		var op = opOptions[opIndex].text;
		
		var kId = document.getElementById("kioskID").innerHTML;
		
		var kname = document.getElementById("kioskName").innerHTML;
		var kserial = document.getElementById("kioskSerial").innerHTML;
		var kip = document.getElementById("kioskIp").innerHTML;
		var knotes = document.getElementById("kioskNote").innerHTML;
		
		$("#opNamek").focus();
		$("#opNamek").val(op);
		$("#kioskId").focus();
		$("#kioskId").val(kId);
		$("#kNameForm").focus();
		$("#kNameForm").val(kname);
		$("#kSerialForm").focus();
		$("#kSerialForm").val(kserial);
		$("#kIpForm").focus();
		$("#kIpForm").val(kip);
		$("#kNotesForm").focus();
		$("#kNotesForm").val(knotes);
	});
	
	$('#modalDeleteKiosk').on('shown.bs.modal', function() {
		var opIndex = document.getElementById("operator").selectedIndex;
		var opOptions = document.getElementById("operator").options;
		var op = opOptions[opIndex].text;
		
		var kId = document.getElementById("kioskID").innerHTML;
		
		var kname = document.getElementById("kioskName").innerHTML;
		var kserial = document.getElementById("kioskSerial").innerHTML;
		var kip = document.getElementById("kioskIp").innerHTML;
		var knotes = document.getElementById("kioskNote").innerHTML;
		
		$("#opNamek_delete").focus();
		$("#opNamek_delete").val(op);
		$("#kioskId_delete").focus();
		$("#kioskId_delete").val(kId);
		$("#kNameForm_delete").focus();
		$("#kNameForm_delete").val(kname);
		$("#kSerialForm_delete").focus();
		$("#kSerialForm_delete").val(kserial);
		$("#kIpForm_delete").focus();
		$("#kIpForm_delete").val(kip);
		$("#kNotesForm_delete").focus();
		$("#kNotesForm_delete").val(knotes);
	});
	
	$('#modalAddKiosk').on('shown.bs.modal', function() {
		var opIndex = document.getElementById("operator").selectedIndex;
		var opOptions = document.getElementById("operator").options;
		var op = opOptions[opIndex].text;
		
		$("#opName_add").focus();
		$("#opName_add").val(op);
	});
	
	$('#modalEditOperator').on('shown.bs.modal', function() {
		var opIndex = document.getElementById("operator").selectedIndex;
		var opOptions = document.getElementById("operator").options;
		var op = opOptions[opIndex].text;
		
		var fname = document.getElementById("opFN").innerHTML;
		var lname = document.getElementById("opLN").innerHTML;
		var uname = document.getElementById("opUN").innerHTML;
		var id = document.getElementById("opId").innerHTML;
		var pid = document.getElementById("opPid").innerHTML;
		var email = document.getElementById("opEmail").innerHTML;
		var phone = document.getElementById("opPhone").innerHTML;
		var opType = document.getElementById("opType").innerHTML;
		
		$("#opName_OE").focus();
		$("#opName_OE").val(op);
		$("#f_name_OE").focus();
		$("#f_name_OE").val(fname);
		$("#l_name_OE").focus();
		$("#l_name_OE").val(lname);
		$("#usr_OE").focus();
		$("#usr_OE").val(uname);
		$("#opID_OE").focus();
		$("#opID_OE").val(id);
		$("#email_OE").focus();
		$("#email_OE").val(email);
		$("#p_id_OE").focus();
		$("#p_id_OE").val(pid);
		$("#phone_OE").focus();
		$("#phone_OE").val(phone);
		//$("#repType_OE").focus();
		//$("#repType_OE").checked = true;
		if (opType == 1){
			document.getElementById("repType_OE1").checked = true;
		}
		if (opType == 2){
			document.getElementById("repType_OE2").checked = true;
		}
		
/*		var opIndex = document.getElementById("operator").selectedIndex;
		var opOptions = document.getElementById("operator").options;
		var op = opOptions[opIndex].text;
		
		var fname = document.getElementById("opFN").innerHTML;
		var lname = document.getElementById("opLN").innerHTML;
		var uname = document.getElementById("opUN").innerHTML;
		var id = document.getElementById("opId").innerHTML;
		var pid = document.getElementById("oppid").innerHTML;
		var email = document.getElementById("opEmail").innerHTML;
		var phone = document.getElementById("opPhone").innerHTML;
		
		$("#opName_OE").focus();
		$("#opName_OE").val(op);
		$("#f_name_OE").focus();
		$("#f_name_OE").val(fname);
		$("#l_name_OE").focus();
		$("#l_name_OE").val(lname);
		$("#usr_OE").focus();
		$("#usr_OE").val(uname);
		$("#opID_OE").focus();
		$("#opID_OE").val(id);
		$("#email_OE").focus();
		$("#email_OE").val(email);
		$("#p_id_OE").focus();
		$("#p_id_OE").val(pid);
		$("#phone").focus();
		$("#phone").val(phone);*/
	});
	

	$('#modalDeleteOperator').on('shown.bs.modal', function() {
		var opIndex = document.getElementById("operator").selectedIndex;
		var opOptions = document.getElementById("operator").options;
		var op = opOptions[opIndex].text;
		
		var fname = document.getElementById("opFN").innerHTML;
		var lname = document.getElementById("opLN").innerHTML;
		var uname = document.getElementById("opUN").innerHTML;
		var id = document.getElementById("opId").innerHTML;
		
		$("#opName_OD").focus();
		$("#opName_OD").val(op);
		$("#f_name_OD").focus();
		$("#f_name_OD").val(fname);
		$("#l_name_OD").focus();
		$("#l_name_OD").val(lname);
		$("#usr_OD").focus();
		$("#usr_OD").val(uname);
		$("#opID_OD").focus();
		$("#opID_OD").val(id);
	});
}

function addNewOperator(){
	
	if (window.XMLHttpRequest) {
		// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {
		// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			document.getElementById("alertMessage").innerHTML = xmlhttp.responseText;
		}
	};
	
	xmlhttp.open("GET","saveData.php?command=saveNewOperator&OpName='Op'&ctcname='cntc'&lastname='lastname'&email='mail'&username='username'&password='passwd'",true);
	xmlhttp.send();
}

function checkPass()
{
	//Store the password field objects into variables ...
	var pass1 = document.getElementById('pwd');
	var pass2 = document.getElementById('conf_pwd');
	//Store the Confimation Message Object ...
	var message = document.getElementById('confirmMessage');
	//Set the colors we will be using ...
	var goodColor = "#66cc66";
	var badColor = "#ff6666";
	//Compare the values in the password field 
	//and the confirmation field
	if(pass1.value == pass2.value){
		//The passwords match. 
		//Set the color to the good color and inform
		//the user that they have entered the correct password 
		pass2.style.backgroundColor = goodColor;
		message.style.color = goodColor;
		message.innerHTML = "Passwords Match!"
	}else{
		//The passwords do not match.
		//Set the color to the bad color and
		//notify the user.
		pass2.style.backgroundColor = badColor;
		message.style.color = badColor;
		message.innerHTML = "Passwords Do Not Match!"
	}
} 
