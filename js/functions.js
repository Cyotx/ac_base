/*(function (global) {
    var DemoViewModel,
        app = global.app = global.app || {};

        checkAvailable: function () {
			cordova.plugins.email.isAvailable(this.callback);
        },

        composeEmail: function () {
			cordova.plugins.email.open({
				to:          ['person1@domain.com'],
				cc:          ['person2@domain.com'],
				bcc:         ['person3@domain.com', 'person4@domain.com'],
				attachments: ['file://img/logo.png'],
				subject:     'EmailComposer plugin test',
				body:        '<h2>Hello!</h2>This is a nice <strong>HTML</strong> email with two attachments.',
				isHtml:      true
			}, this.callback)
        },

        callback: function(msg) {
            navigator.notification.alert(JSON.stringify(msg), null, 'EmailComposer callback', 'Close');
        },
    });*/


var bodyContent = "";

function formEmailBody(){
	bodyContent += "<!DOCTYPE html><html><body>";
	bodyContent += "<h1>AMERICAN CHANGER CORP</h1>";
	bodyContent += "<font size=3 color='blue' face='arial black'>General System Report <font>";
	bodyContent += "<p>System Status: OK</p>";
	bodyContent += "<p>This message has been sent from an android application</p>";
	bodyContent += "</body></html>";
}

function callback(msg){
	navigator.notification.alert(JSON.stringify(msg), null, 'EmailComposer callback', 'Close');
}

function checkAvailable(){
	cordova.plugins.email.isAvailable(callback);
}

function composeEmail() {
	formEmailBody();
	alert('Preparing to send Report to E-mail');

	if (!cordova.plugins.email.isAvailable()){
		cordova.plugins.email.open({
			to:      'ale3191@gmail.com',
			attachments: ['file://img/banner_american_changer.jpg', 'res://icon.png',
							'file://img/report.pdf'],
			subject: 'American Changer Corp. Report',
			body:   bodyContent,
			isHtml:  true
		},callback);

	}
	else {
		alert('Service is not available');
	}
}